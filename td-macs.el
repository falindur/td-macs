;;; package --- td-macs  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:
(mapcar #'require '(td-macs-base td-macs-entities))

(defun td-macs--xref-identifier-completion-table (&rest args)
  "This allows for only completing on td-macs functions when in
xref-find-definitions.  To use this a .dir-locals.el file could
be made with an entry of 
(elisp--xref-identifier-completion-table . td-macs--xref-identifier-completion-table)"
  (apply (apply-partially #'completion-table-with-predicate
						  obarray
						  (lambda (sym)
							(and
							 (or (boundp sym)
								 (fboundp sym)
								 (featurep sym)
								 (facep sym))
							 (string-match (rx (zero-or-more anything) "td-macs" (zero-or-more anything)) (symbol-name sym))))
						  'strict)
		 args))

(defgroup td-macs nil
  "Group for customising Tower Defence"
  :prefix "td-macs-"
  :group 'games)

(defcustom td-macs-tick-period 0.016
  "The time taken between the step cycles of the game."
  :type 'number)

(defcustom td-macs-default-health 5
  "The amount of health a session starts with."
  :type 'number)

(defcustom td-macs-default-creeps-amount 10
  "The amount of creeps a wave should have by default."
  :type 'number)

(defcustom td-macs-default-graphics-scale 0
  "The amount with which the graphics in the td-macs buffers are scaled."
  :type 'number)

(defcustom td-macs-buffer-name "*TD MACS*"
  "Name used for td-macs game buffer."
  :type 'string)

(defcustom td-macs-stats-buffer-name "*TD MACS STATS*"
  "Name used for td-macs statistics buffer."
  :type 'string)

(defcustom td-macs-score-file (concat gamegrid-user-score-file-directory "td-macs-scores")
  "File location for saving the scores."
  :type 'file)

(defface td-macs-entity-hit '((t . (:foreground "#ff0000")))
  "Face for when a entity gets damaged.")

(defface td-macs-entity-poison '((t . (:foreground "#3fb83f")))
  "Face for when a entity gets poisoned.")

(defface td-macs-entity-slow '((t . (:inherit 'ansi-color-cyan
									 :distant-foreground "black")))
  "Face for when a entity gets slowed.")

(defface td-macs-entity-haste '((t . (:inherit 'ansi-color-bright-yellow
									  :distant-foreground "black")))
  "Face for when a entity goes faster than normal.")

(defface td-macs-highlight-entity '((t . (:inherit 'highlight)))
  "Face for when a entity gets highlighted.")

(defface td-macs-modifier-fire-one '((t . (:foreground "#ffa500")))
  "Face for the fire modifier.")

(defface td-macs-modifier-fire-two '((t . (:foreground "#ff8c00")))
  "Face for the fire modifier.")

(defface td-macs-modifier-fire-three '((t . (:foreground "#ff4500")))
  "Face for the fire modifier.")

(defcustom td-macs-default-map
  '((?# ?# ?# ?# ?0 ?# ?# ?# ?#)
	(?# ?# ?# ?# ?. ?# ?# ?# ?#)
	(?# ?# ?# ?# ?. ?# ?# ?# ?#)
	(?# ?# ?# ?# ?. ?# ?# ?# ?#)
	(?# ?# ?# ?# ?. ?. ?. ?# ?#)
	(?# ?# ?# ?# ?# ?# ?. ?# ?#)
	(?# ?# ?# ?# ?# ?# ?. ?. ?#)
	(?# ?# ?# ?# ?# ?# ?# ?. ?#)
	(?# ?# ?# ?# ?# ?# ?. ?. ?#)
	(?# ?# ?# ?# ?X ?. ?. ?# ?#))
  "Default map to play on."
  ;;ehm
  :type '(list (list integer integer integer integer integer integer integer integer integer)
			   (list integer integer integer integer integer integer integer integer integer)
			   (list integer integer integer integer integer integer integer integer integer)
			   (list integer integer integer integer integer integer integer integer integer)
			   (list integer integer integer integer integer integer integer integer integer)
			   (list integer integer integer integer integer integer integer integer integer)
			   (list integer integer integer integer integer integer integer integer integer)
			   (list integer integer integer integer integer integer integer integer integer)
			   (list integer integer integer integer integer integer integer integer integer)
			   (list integer integer integer integer integer integer integer integer integer)))

(defcustom td-macs-default-creep-waves (list (cons td-macs-default-creeps-amount 'make-td-macs-creep)
											 (cons 15 'make-td-macs-creep-runner)
											 (cons td-macs-default-creeps-amount 'make-td-macs-creep)
											 (cons td-macs-default-creeps-amount 'make-td-macs-creep)
											 (cons 25 'make-td-macs-creep-runner)
											 (cons td-macs-default-creeps-amount 'make-td-macs-creep))
  "Default list of creep waves."
  :type '(alist :key-type integer :value-type symbol))

(defcustom td-macs-default-endless-creep-waves (list (cons td-macs-default-creeps-amount 'make-td-macs-creep)
													 (td-macs--creep-generator 15 'make-td-macs-creep-runner))
  "Default list for endless creep waves. Can be used to see how endless mode is implemented."
  :type '(alist :key-type integer :value-type symbol))

(defvar td-macs-test-map2
  '((?0 ?. ?. ?. ?. )
	(?# ?# ?# ?# ?. )
	(?# ?# ?# ?# ?. )
	(?# ?# ?# ?# ?. )
	(?X ?. ?. ?. ?. )))

(defvar td-macs-intersection-map
  '((?# ?# ?0 ?# ?#)
   (?# ?# ?. ?# ?#)
   (?X ?. (?. (0 1) (-1 0)) ?. ?.)
   (?# ?# ?. ?# ?.)
   (?# ?# ?. ?. ?.)))

(defcustom td-macs-playable-levels '(("default" . (:waves td-macs-default-creep-waves
												   :level td-macs-default-map
												   :crystals 3))
									 ("test2" . (:waves td-macs-default-creep-waves
												 :level td-macs-test-map2
												 :crystals 2))
									 ("intersection" . (:waves td-macs-default-creep-waves
														:level td-macs-intersection-map
														:crystals 5))
									 ("endless" . (:waves td-macs-default-endless-creep-waves
												   :level td-macs-default-map
												   :crystals 6)))
  "List of levels which can be played in a session."
  :type '(alist :key-type string :value-type (plist :key-type symbol :value-type symbol)))

(defvar-keymap td-macs-mode-map
  :doc "Keymap for tower defence games."
  :name 'td-macs-mode-map
  "p" #'td-macs-pause-game
  "n" #'td-macs-start-session
  "s" #'td-macs-start-wave
  "q" #'td-macs-game-over
  "t" #'td-macs-tower-dispatch-menu

  ;; movement
  "h" #'td-macs-player-move-left
  "j" #'td-macs-player-move-down
  "k" #'td-macs-player-move-up
  "l" #'td-macs-player-move-right
  "<left>" #'td-macs-player-move-left
  "<down>" #'td-macs-player-move-down
  "<up>" #'td-macs-player-move-up
  "<right>" #'td-macs-player-move-right)

(defvar-keymap td-macs-mode-game-over-map
  :doc "Keymap for tower defence games."
  :name 'td-macs-mode-game-over-map
  "n" #'td-macs-start-session
  "q" #'td-macs-quit-window)

(defvar td-macs-blank-code ? )
(defvar td-macs-ground-code ?#)
(defvar td-macs-path-code ?.)
(defvar td-macs-spawn-code ?0)
(defvar td-macs-fortress-code ?X)

(defvar-local td-macs-game-state '(:level ()
								   :health 0
								   :score 0
								   :crystals 0
								   :creeps ()
								   :waves-survived 0
								   :current-wave ()
								   :waves ()
								   :creeps-amount 0
								   :towers ()
								   :spawn ()
								   :fortress ()
								   :player ()
								   :creep-path ()))

(defvar td-macs-current-session-buffer nil)
(defvar td-macs-paused nil)
(defvar-local td-macs-wave-started nil)

(defun td-macs--player-move-by-pos (pos)
  (td-macs-with-plist (player level) td-macs-game-state
	(let ((player-pos (td-macs--entity-pos player))
		   (bounds (if (cl-plusp (apply '+ pos))
					   (list 'min (td-macs--level-inner-bounds
								   level))
					 (list 'max '(0 0)))))
	  (setf (car (td-macs-entity-pos player))
			(seq-mapn (car bounds) (cadr bounds)
					  (seq-mapn '+ player-pos pos))))))

(defun td-macs-player-move-left ()
  (interactive nil td-macs-mode)
  (td-macs--player-move-by-pos '(-1 0)))

(defun td-macs-player-move-right ()
  (interactive nil td-macs-mode)
  (td-macs--player-move-by-pos '(1 0)))

(defun td-macs-player-move-down ()
  (interactive nil td-macs-mode)
  (td-macs--player-move-by-pos '(0 1)))

(defun td-macs-player-move-up ()
  (interactive nil td-macs-mode)
  (td-macs--player-move-by-pos '(0 -1)))

(defun td-macs--spawn-empty-of-creep-p ()
  "Tells if the spawn is free of creeps."
  (td-macs-with-plist (spawn creeps) td-macs-game-state
	(null (seq-some (td-macs--entity-pos-eq-p spawn) creeps))))

(defun td-macs--creep-spawn (&optional position)
  "Spawns/constructs a CREEP based on the current wave."
  (td-macs-with-plist (current-wave) td-macs-game-state
	(funcall (cdr current-wave) :pos (or position (cons '(0 . 0) 0)) :health 5)))

(defun td-macs--creep-spawn-on-level ()
  "Spawns a CREEP on the spawn entity in the current level."
  (td-macs-with-plist (creeps-amount creeps spawn) td-macs-game-state
	(when (and (> creeps-amount 0)
			   (td-macs--spawn-empty-of-creep-p))
	  (setf creeps-amount (1- creeps-amount))
	  (push (td-macs--creep-spawn (cons spawn 0)) creeps))))

(defun td-macs-creep-hit-fortress (creep)
  (td-macs-with-plist (fortress creeps health) td-macs-game-state
	(when (equal (td-macs--entity-pos creep) fortress)
	  (setf creeps (nbutlast creeps 1))
	  (setf health (1- health))
	  t)))

(defun td-macs--draw-cell (code x y)
  "Draws the CODE at the position of X and Y.
Internally it translates X and Y to buffer positions."
  (let ((buffer-read-only nil))
	(goto-char (td-macs--pos-to-point x y))
	(delete-char 1)
	(insert-char code 1)))

(defun td-macs--code-at-pos (pos)
  "Returns the code of the entity at POS."
  (cl-destructuring-bind (x y) pos
    (char-after (td-macs--pos-to-point x y))))

(defun td-macs--pos-to-point (x y)
  "Transforms the X and Y to a point position in the buffer."
  (td-macs-with-plist (level) td-macs-game-state
	(+ 1
	   (* (1+ (length (car level))) y)
	   x)))

(cl-defgeneric td-macs--entity-apply-face (obj-to-apply face-symbol)
  (:documentation "Applies the FACE of FACE-SYMBOL to OBJ-TO-APPLY."))

(cl-defmethod td-macs--entity-apply-face ((entity td-macs-entity) face-symbol)
  (save-excursion
	(let ((buffer-read-only nil))
	  (goto-char (apply 'td-macs--pos-to-point (td-macs--entity-pos entity)))
	  (add-face-text-property (point) (1+ (point)) face-symbol 'append))))

(defun td-macs--highlight-entity (entity)
  "Highlights the position of the ENTITY."
  (td-macs--entity-apply-face entity 'td-macs-highlight-entity))

(defun td-macs--highlight-bounds (bounds)
  "Highlights the positions inside the BOUNDS.  If the BOUNDS
surpass `td-macs--level-inner-bounds' then it will crop to the
level BOUNDS."
  (save-excursion
	(td-macs-with-plist (level) td-macs-game-state
	  (let ((buffer-read-only nil)
			(min-y (td-macs--bounds-min-y bounds))
			(max-x (td-macs--bounds-max-x bounds))
			(check-point (lambda (num) (max (point-min) (min (point-max) num)))))
		(dotimes (offset-y (- (1+ (td-macs--bounds-max-y bounds)) min-y))		  
		  (let ((min-point (goto-char (funcall check-point
											   (td-macs--pos-to-point
												(td-macs--bounds-min-x bounds)
												(+ min-y offset-y))))))
		  
			(add-face-text-property min-point
									(goto-char (funcall check-point 
														(1+ (td-macs--pos-to-point max-x
																				  (+ min-y offset-y)))))
									'td-macs-highlight-entity 'append)))))))

(defun td-macs--creep-move (creep)
  (let ((path (plist-get td-macs-game-state :creep-path)))
	(setf (td-macs-entity-pos creep)
		  (cons (nth (cl-incf (td-macs-creep-path-pos creep)
						   (td-macs--incf-slot-by-speed creep td-macs-entity-pos 1))
				  path)
				(cdr (td-macs-entity-pos creep))))))

(defun td-macs-creeps-step ()
  ;;we loop now 3 times with many it may be slow?
  ;;dispatch over the creep instead of two loops. But then we need to redefine the creep-hit-fortress func logic
  (td-macs--creep-spawn-on-level)
  (td-macs-with-plist (creeps) td-macs-game-state
	(seq-do 'td-macs--entity-step creeps)))

(defun td-macs--destroy-creep (creep)
  "For now we actually destroy the creep in the future we may do object pool with visible flag"
  (td-macs-with-plist (creeps score crystals) td-macs-game-state
    (setf creeps (remove creep creeps)
		  score (1+ score)
		  crystals (+ crystals (td-macs-creep-crystals creep)))))

(defun td-macs-towers-step ()
  (td-macs-with-plist (towers) td-macs-game-state
	(seq-do 'td-macs--entity-step towers)))

(defun td-macs-game-draw (game-state)
  (td-macs-with-plist (level creeps towers player) game-state
    (td-macs-level-draw level)
	(td-macs--stats-draw)
	(mapc 'td-macs--draw creeps)
	(mapc 'td-macs--draw towers)
	(td-macs--draw player)))

(defun td-macs-game-step (td-buffer)
  (td-macs-with-plist (health creeps-amount creeps) td-macs-game-state
	(cond
	 ((not (eq (current-buffer) td-macs-current-session-buffer))) ;;we do nothing
	 ((<= health 0)
	  (td-macs-game-draw td-macs-game-state)
	  (td-macs-game-over))
	 ((and (<= creeps-amount 0) (seq-empty-p creeps))
	  (td-macs-game-draw td-macs-game-state)
	  (td-macs--wave-finished))
	 ((not td-macs-wave-started) (td-macs-game-draw td-macs-game-state))
	 (td-macs-paused
	  (td-macs-game-draw td-macs-game-state))
	 ((not td-macs-paused)
	  (td-macs-creeps-step)
	  (td-macs-towers-step)
	  (td-macs-game-draw td-macs-game-state)))))

(defun td-macs-level-draw (level)
  (seq-do-indexed
   (lambda (row y) (seq-do-indexed (lambda (code x) (td-macs--draw-cell code x y)) row))
   level))

(defun td-macs--text-draw (buff text-lines)
  (with-current-buffer buff
	(erase-buffer)
	(apply 'insert text-lines)))

(defun td-macs--stats-draw ()
  (td-macs--text-draw td-macs-stats-buffer-name
	(list (td-macs--stats-general-text)
		  (td-macs--stats-wave-text)
		  (td-macs--stats-object-text))))

(defun td-macs--object-at-player-pos ()
  (td-macs-with-plist (player towers creeps) td-macs-game-state
	(let ((at-player? (td-macs--entity-pos-eq-p player)))
	  (or (seq-some at-player? towers)
		  (seq-some at-player? creeps)))))

(defun td-macs--stats-object-text ()
  "Gives back a formatted string for the object at the PLAYER's position in `td-macs-game-state'."
  (format "\nObject information:\n [%s]" (td-macs-print-string (td-macs--object-at-player-pos))))

(defun td-macs--stats-general-text ()
  "Gives back a formatted string for the necessary stats."
  (td-macs-with-plist (health creeps-amount score crystals) td-macs-game-state
	(format "Stats:\nHealth: %03d\nCreeps: %03d\nScore: %03d\nCrystals: %03d\n"
			health creeps-amount score crystals)))

(defun td-macs--stats-wave-text ()
  "Gives back a formatted string for the current wave stats."
  (td-macs-with-plist (waves-survived current-wave) td-macs-game-state
	(format "\nWave:\nSurvived: %03d\nCurrent creep:\n [%s]\n"
			waves-survived (td-macs-print-string (td-macs--creep-spawn)))))

(cl-defgeneric td-macs--entity-pos-eq-p (to-comp)
  (:documentation "Return a predicate function.
Where the argument should be a ENTITY and their position is
compared with the position of TO-COMP."))

(cl-defmethod td-macs--entity-pos-eq-p ((entity-to-comp td-macs-entity))
  "Return a predicate function.
Where ENTITY-TO-COMP's position is compared with the position of
the entity that is passed to the predicate.  The predicate
function returns nil if they are not equal.  If they are equal it
returns the entity that was passed to the predicate function."
  (lambda (entity-second)
	(when (equal (td-macs--entity-pos entity-to-comp) (td-macs--entity-pos entity-second))
	  entity-second)))

(cl-defmethod td-macs--entity-pos-eq-p ((pos-to-compare list))
  "Return a predicate function.
Where POS-TO-COMPARE is a position with an X and Y element that
is compared with the position of the entity that is passed to the
predicate.  The predicate function returns nil if they are not
equal.  If they are equal it returns t."
  (lambda (entity)
	(equal pos-to-compare (td-macs--entity-pos entity))))

(defun td-macs--path-next-by-list (pos level)
  "Gives back the next path postion based on the contents at POS in
LEVEL.  This function alters the content at POS."
  (cl-destructuring-bind (x y) pos
	(let* ((content (nth x (nth y level)))
		   (move (cadr content)))
	  (setf (cdr (nth x (nth y level))) (cddr content))
	  (seq-mapn '+ pos move))))

(defun td-macs--path-next-by-code (pos last-pos level)
  "Gives back what the next path position is based on current POS
and LAST-POS in LEVEL."
  (let* ((x (car pos))
		 (y (cadr pos))
		 (directions `(,(if (< (1- x) 0) (list nil 'x)
						  (list (list (1- x) y) (td-macs--entity-code-at-pos (list (1- x) y) level))) ;;left
					   ,(list (list (1+ x) y) (td-macs--entity-code-at-pos (list (1+ x) y) level))	 ;;right
					   ,(if (< (1- y) 0) (list nil 'x)
						  (list (list x (1- y)) (td-macs--entity-code-at-pos (list x (1- y)) level))) ;;up
					   ,(list (list x (1+ y)) (td-macs--entity-code-at-pos (list x (1+ y)) level))	 ;;down
					   )))
	(setq directions (assoc-delete-all last-pos directions))
	(setq directions (assoc-delete-all nil directions))
	(car (rassoc (list td-macs-path-code) directions))))

(defun td-macs-path-next (pos last-pos level)
  "Gives back what the next path position is based on current POS
and LAST-POS in LEVEL."
  (cl-destructuring-bind (x y) pos
	(let ((content (nth x (nth y level))))
	  (if (listp content)
		  (td-macs--path-next-by-list pos level)
		(td-macs--path-next-by-code pos last-pos level)))))

(defun td-macs-creep-path (level)
  "Gives back the path that creeps will take in LEVEL."
  ;; For now we only accept one spawn and one fortress
  ;; and only in four directions not 8
  (let* ((level-copy (copy-tree level))
		 (path (list (td-macs-spawn level-copy)))
		 (fortress (td-macs-fortress level-copy)))
	(while (not (null (car path)))
	  (push (td-macs-path-next (car path) (cadr path) level-copy) path))
	(setf (car path) fortress)
	(nreverse path)))

(defun td-macs-level-collect-entity (level entity)
  (let ((collect '()))
	(seq-do-indexed
	 (lambda (row row-index)
	   (seq-do-indexed
		(lambda (col col-index)
		  (when (equal col entity)
			(push (list col-index row-index) collect)))
		row))
	 level)
	collect))

(defun td-macs-spawn (level)
  (car (td-macs-level-collect-entity level td-macs-spawn-code)))

(defun td-macs-fortress (level)
  (car (td-macs-level-collect-entity level td-macs-fortress-code)))

(defun td-macs--clear-buffer (level)
  "Clears the buffer and draws an empty canvas based on the bounds of LEVEL."
  (let ((blank-line (concat
					 (make-string (length (car level))
								  td-macs-blank-code)
					 "\n"))
		(buffer-read-only nil))
	(erase-buffer)
	(dotimes (row (length level))
	  (insert blank-line))))

(defun td-macs-init-grid (level)
  (td-macs--clear-buffer level)
  (td-macs-level-draw level))

(defun td-macs--game-won ()
  (use-local-map td-macs-mode-game-over-map)
  (gamegrid-kill-timer)
  (setq td-macs-paused nil)
  (setq-local td-macs-wave-started nil)
  (td-macs-save-score)
  (message "YOU WON!"))

(defun td-macs-game-over ()
  (interactive nil td-macs-mode)
  (td-macs-reset-game)
  (use-local-map td-macs-mode-game-over-map)
  (message "You lost!")
  (when (y-or-n-p "Want to save your score?")
	(td-macs-save-score)))

(defun td-macs-pause-game ()
  "Pause (or resume) the current game."
  (interactive nil td-macs-mode)
  (setq td-macs-paused (not td-macs-paused)))

(defun td-macs-tower-dispatch-menu (&optional choice)
  (interactive nil td-macs-mode)
  (td-macs-with-plist (player level towers) td-macs-game-state
	(let ((pos (td-macs--entity-pos player))
		  (tower (seq-some (td-macs--entity-pos-eq-p player) towers)))
	  (cond ((and (= td-macs-ground-code (nth (td-macs-pos-x pos)
											  (nth (td-macs-pos-y pos) level)))
				  (not tower))
			 (td-macs--tower-insert choice))
			(tower (td-macs--tower-menu tower))
		  	(t (message "You are not able to place a tower here!"))))))

(defun td-macs--tower-insert (&optional choice)
  "Inserts a tower based on the CHOICE of the user.
Tower is inserted at the PLAYER's position in `td-macs-game-state'."
  (interactive nil td-macs-mode)
  (td-macs-with-plist (player towers) td-macs-game-state
	(let* ((choice (or choice (read-char (td-macs-tower-insert-dispatch-help-string))))
		   (pos (td-macs--entity-pos player))
		   (tower (td-macs--tower-make (cdr (assoc choice td-macs-tower-insert-action-alist)) pos)))
	  (when (not (null tower)) (push tower towers)))))

(defun td-macs--tower-menu (tower)
  (let* ((input (read-char (td-macs-tower-menu-dispatch-help-string tower)))
		 (choice (assoc input (td-macs--tower-menu-action-alist tower))))
	(apply (cadr choice)
		   (list tower (cddr choice)))))

(defun td-macs--wave-cdr (waves)
  (cond ((functionp (cadr waves)) (cons (funcall (cadr waves)) (list (cadr waves))))
		(t (cdr waves))))

(defun td-macs--wave-swap-new-wave ()
  (td-macs-with-plist (creeps-amount current-wave waves waves-survived) td-macs-game-state
	(let ((next-wave (car waves)))
	  (setf creeps-amount (car next-wave))
	  (setf current-wave next-wave)
	  (cl-incf waves-survived)
	  (setf waves (td-macs--wave-cdr waves)))))

(defun td-macs--wave-finished ()
  (setq td-macs-paused nil)
  (setq td-macs-wave-started nil)
  (td-macs-with-plist (waves) td-macs-game-state
	(if waves
		(progn (td-macs--wave-swap-new-wave)
			   (message "You survived this time!"))
	  (td-macs--game-won))))

(defun td-macs-reset-game ()
  (gamegrid-kill-timer)
  (setq td-macs-paused nil)
  (setq-local td-macs-wave-started nil)
  (setq-local td-macs-game-state (td-macs--game-state-empty)))

(defun td-macs--game-state-empty ()
  (list :level '()
		:health 0
		:crystals 0
		:score 0
		:creeps '()
		:waves-survived 0
		:current-wave '()
		:waves '()
		:creeps-amount 0
		:towers '()
		:spawn '()
		:fortress '()
		:player '()
		:creep-path '()))

(defun td-macs-start-wave (&optional state)
  (interactive nil td-macs-mode)
  (setq-local td-macs-wave-started t))

(defun td-macs-start-game (&optional state)
  (interactive nil td-macs-mode)
  (td-macs-reset-game)
  (td-macs-init-game state)
  (use-local-map td-macs-mode-map)
  (td-macs-start-level (or (when state (plist-get state :level))
						   td-macs-default-map)))

(defun td-macs-init-game (&optional state)
  (setq-local td-macs-game-state
			  (or state	(td-macs--game-state-default))))

(defun td-macs--game-state-default ()
  (let ((cur-wave (car td-macs-default-creep-waves)))
	(list :level td-macs-default-map
		  :creep-path (td-macs-creep-path td-macs-default-map)
		  :spawn (td-macs-spawn td-macs-default-map)
		  :fortress (td-macs-fortress td-macs-default-map)
		  :crystals 0
		  :towers '()
		  :waves td-macs-default-creep-waves
		  :waves-survived 0
		  :current-wave cur-wave
		  :creeps-amount (car cur-wave)
		  :creeps '()
		  :health td-macs-default-health
		  :player (make-td-macs-player :pos (cons '(0 0) 0))
		  :score 0)))

(defun td-macs--calibrate-state (state)
  (td-macs-with-plist (waves level) state
	(let ((cur-wave (car waves))
		  (stripped-level (td-macs--level-strip level)))
	  (map-merge 'plist state
				 (list :creep-path (td-macs-creep-path level)
					   :level stripped-level
					   :spawn (td-macs-spawn stripped-level)
					   :fortress (td-macs-fortress stripped-level)
					   :current-wave cur-wave
					   :creeps-amount (car cur-wave))))))

(defun td-macs-save-score ()
  (interactive nil td-macs-mode)
  (gamegrid-add-score td-macs-score-file (plist-get td-macs-game-state :score)))

(defun td-macs-save-game-state (file)
  (interactive "FFile: " td-macs-mode)
  (let ((state td-macs-game-state)
		(buff (create-file-buffer file)))
	(with-current-buffer buff
	  (print state buff)
	  (write-region nil nil file)
	  (kill-buffer buff))))

(defun td-macs-start-timer ()
  (gamegrid-start-timer td-macs-tick-period 'td-macs-game-step))

(defun td-macs-start-level (level)
  (gamegrid-kill-timer)
  (td-macs-init-grid level)
  (td-macs-start-timer))

(defun td-macs--setup-buffer-vars ()
  (setq buffer-read-only t
		truncate-lines t
		line-spacing 0
		cursor-type nil)
  (buffer-disable-undo (current-buffer)))

(defun td-macs--scale-graphics (buffer-name)
  (with-current-buffer buffer-name
	(text-scale-increase 0)
	(text-scale-increase td-macs-default-graphics-scale)))

(define-derived-mode td-macs-mode nil "TD-MACS"
  "A mode for playing tower defence."
  (add-hook 'kill-buffer-hook #'gamegrid-kill-timer nil t)
  (td-macs--setup-buffer-vars)
  (td-macs--scale-graphics td-macs-buffer-name)
  (td-macs--scale-graphics td-macs-stats-buffer-name))

(defun td-macs--init-session ()
  (select-window (or (get-buffer-window td-macs-buffer-name)
					 (selected-window)))
  (gamegrid-kill-timer)
  (switch-to-buffer td-macs-buffer-name)
  (select-window (or (get-buffer-window td-macs-stats-buffer-name)
					 (split-window-right nil (get-buffer-window td-macs-buffer-name))))
  (switch-to-buffer td-macs-stats-buffer-name)
  (select-window (get-buffer-window td-macs-buffer-name))
  (setq td-macs-current-session-buffer (current-buffer))
  (td-macs-mode))

(defun td-macs--entity-code (code-or-complex)
  (if (numberp code-or-complex) code-or-complex
	(car code-or-complex)))

(defun td-macs--entity-code-at-pos (pos level)
  "Returns the entity code at POS in LEVEL."
  (cl-destructuring-bind (x y) pos
	(let ((content (nth x (nth y level))))
	  (td-macs--entity-code content))))

(defun td-macs--level-strip (level)
  "Strips down the level to have only entity codes."
  (seq-map
   (lambda (row)
	 (seq-map (lambda (col)
				(td-macs--entity-code col))
			  row))
   level))

(defun td-macs--level-get (level-name)
  (seq-map (lambda (entry)
			 (if (symbolp entry) (symbol-value entry)
			   entry))
		   (cdr (assoc level-name td-macs-playable-levels))))

(defun td-macs-start-session (level-selection)
  "Function to start tower-defence game session."
  (interactive (list (completing-read "Level: " td-macs-playable-levels)))
  (if-let (level (td-macs--level-get level-selection))
	(progn (td-macs--init-session)
		   (td-macs-start-game (td-macs--calibrate-state
								(map-merge 'plist
										   (td-macs--game-state-default)
										   level))))
	(user-error "Couldn't start a Tower defence session because the level selected: \"%s\" does not exist!" level-selection)))

(defun td-macs--start-session-from-state (state)
  "Allows for starting a session by a predefined STATE."
  (td-macs--init-session)
  (td-macs-start-game state)
  (td-macs-pause-game)
  (td-macs-game-draw state))

(defun td-macs-from-save-file (file)
  "Function to start tower-defence game"
  (interactive "FSave file: ")
  (let ((state
		 (with-temp-buffer
		   (insert-file-contents file)
		   (read (current-buffer)))))
	(td-macs--start-session-from-state state)))

(defun td-macs-quit-window ()
  (interactive nil td-macs-mode)
  (delete-window (get-buffer-window td-macs-stats-buffer-name))
  (quit-window nil (get-buffer-window td-macs-buffer-name)))

(provide 'td-macs)

;;; td-macs.el ends here
