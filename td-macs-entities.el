;;; package --- td-macs-entities  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:
(require 'td-macs-base)

;;;; generic entity
(cl-defstruct td-macs-entity code
			  (pos '((0 0) . 0) :documentation "CAR represents the position location in x, y. 
 CDR represents the progress to the next move.")
			  (speed 0)
			  (modifiers '() :documentation "This represents a list of modifiers currently on the entity.
Modifiers are instances not symbols.")
			  (health 0 :documentation "The health of the entity, values above 0 represents that the
entity is still alive.")
			  (resistances '() :documentation "Association list representing the resistances to the damage types of towers. 
In the form of '((DMGTYPE1 . PERCENTAGE) (DMGTYPE2 . PERCENTAGE))"))

(cl-defmethod td-macs--draw ((entity td-macs-entity))
  "Function that draws the entity in the buffer."
  (with-slots (code modifiers) entity
	(apply 'td-macs--draw-cell code
					  (td-macs--entity-pos entity))
	(mapc 'td-macs--draw modifiers)))

(defmacro td-macs--inc-slot-by-speed (entity slotfunc)
  `(+ (* td-macs-tick-period (td-macs-entity-speed ,entity))
	  (cdr (,slotfunc ,entity))))

(defmacro td-macs--incf-slot-by-speed (entity slotfunc upper-bound)
  `(let ((prog (td-macs--inc-slot-by-speed ,entity ,slotfunc)))
	 (if (>= prog 1)
		 (setf (,slotfunc ,entity) (cons (car (,slotfunc ,entity)) 0))
	   (setf (,slotfunc ,entity) (cons (car (,slotfunc ,entity)) prog)))
	 (truncate prog)))

(cl-defgeneric td-macs--name (entity))
(cl-defgeneric td-macs-print-string (entity))

(cl-defmethod td-macs-print-string (entity nil)
  "Currently no object selected.")

(cl-defmethod td-macs-print-string ((entities cons))
  (seq-map (lambda (entity) (concat "\t\t" (td-macs-print-string entity) "\n")) entities))

(cl-defmethod td-macs-print-string :around ((entity td-macs-entity))
  (concat (cl-call-next-method entity)
		  (when-let (modifiers (td-macs-entity-modifiers entity))
			(apply 'concat "\n\tModifiers:\n"
				   (td-macs-print-string modifiers)))))

(cl-defgeneric td-macs--entity-step (entity))

(cl-defmethod td-macs--entity-step ((entity td-macs-entity))
  ;;general method that does not do anything
  )

(cl-defmethod td-macs--entity-step :after ((entity td-macs-entity) &rest args)
  (seq-do 'td-macs--modifier-step (td-macs-entity-modifiers entity)))

(defun td-macs--entity-pos (entity)
  (car (td-macs-entity-pos entity)))

(defun td-macs--entity-bounds (entity slotfunc)
  (list (td-macs--entity-pos entity) (funcall slotfunc entity)))

(defun td-macs--entity-within-bounds-p (bounds)
  (lambda (other)
	(td-macs-within-bounds-p (td-macs--entity-pos other) bounds)))

;;; modifiers
(defclass td-macs-modifier ()
  ((duration :initarg :duration
			 :initform '(0 0 0 0)
			 :accessor td-macs-modifier-duration
			 :documentation "Format is the time format of emacs.")
   (total-duration :initarg :total-duration
				   :initform '(99 99 99 99)
				   :accessor td-macs-modifier-total-duration
				   :documentation "This is the total-duration, which is set by the code.")
   (receiver :initarg :receiver
			 :initform nil
			 :accessor td-macs-modifier-receiver
			 :documentation "Entity which the modifier acts upon."))
  (:documentation "Modifiers allow for effects to happen on a certain entity in a
specific duration."))

(cl-defmethod td-macs--draw ((modifier td-macs-modifier)))

(cl-defmethod td-macs--entity-apply-face ((modifier td-macs-modifier) face-symbol)
  "Dispatches the apply face to the RECEIVER of MODIFIER."
  (with-slots (receiver) modifier
	(td-macs--entity-apply-face receiver face-symbol)))

(cl-defmethod td-macs--name ((modifier td-macs-modifier))
  "Returns the name of the modifier as a string."
  "Modifier")

(cl-defmethod td-macs-print-string ((modifier td-macs-modifier))
  (format "%s, Duration: %S" (td-macs--name modifier)
		  (time-to-seconds (td-macs-modifier-duration modifier))))

(defun td-macs--modifier-update-duration (modifier time)
  "Updates the DURATION slot of MODIFIER by TIME."
  (with-slots (duration) modifier
	(setf duration (time-subtract duration time))))

(defun td-macs--modifier-duration-over-p (modifier)
  (or (time-less-p (td-macs-modifier-duration modifier) '(0 0 0 0))
	  (time-equal-p (td-macs-modifier-duration modifier) '(0 0 0 0))))

(cl-defgeneric td-macs--add (original addition))

(cl-defmethod td-macs--add ((entity td-macs-entity) (added-modifiers list))
  "Adds the ADDED-MODIFIERS to the MODIFIERS slot of ENTITY."
  (seq-do (lambda (modifier)
			(td-macs--add modifier entity)
			(td-macs--add entity modifier))
		  added-modifiers))

(cl-defmethod td-macs--add ((entity td-macs-entity) (modifier td-macs-modifier))
  "Adds the MODIFIER to the MODIFIERS slot of ENTITY."
  (with-slots (modifiers) entity
	(setf modifiers (cons modifier modifiers))))

(cl-defmethod td-macs--add ((modifier td-macs-modifier) (entity td-macs-entity))
  "Adds the ENTITY as receiver on MODIFIER."
  (setf (td-macs-modifier-receiver modifier) entity))

(defun td-macs--modifier-ensure-total-duration (modifier)
  (with-slots (total-duration duration) modifier
	(when (equal total-duration '(99 99 99 99)) (setf total-duration duration))))

(cl-defgeneric td-macs--cleanup (entity))

(cl-defmethod td-macs--cleanup ((modifier td-macs-modifier))
  (with-slots (modifiers) (td-macs-modifier-receiver modifier)
	(setf modifiers (remove modifier modifiers))))

(defun td-macs--modifier-step (modifier)
  (td-macs--modifier-ensure-total-duration modifier) ;; don't know if this is the best way
  (if (td-macs--modifier-duration-over-p modifier)
	  (td-macs--cleanup modifier)
	  (td-macs--modifier-receiver-step modifier))
  (td-macs--modifier-update-duration modifier (seconds-to-time td-macs-tick-period)))

(cl-defgeneric td-macs--modifier-receiver-step (modifier)
  (:method (modifier)))

(defclass td-macs-highlight-mixin ()
  ((face :initarg :face :initform 'error))
  (:documentation "Mixin that highlights SELF after the primary draw method. 
For this it uses the FACE slot."))

(cl-defmethod td-macs--draw :after ((entity td-macs-highlight-mixin))
  "Highlight ENTITY based on the face slot."
  (with-slots (face) entity
   (td-macs--entity-apply-face entity face)))

(defclass td-macs-modifier-hit (td-macs-modifier td-macs-highlight-mixin)
  ((face :initform 'td-macs-entity-hit)
   (dmg :initform 0 :initarg :dmg
		:accessor td-macs-modifier-hit--dmg))
  (:documentation "Modifier for when a entity gets `hit`, e.g., it gets damaged.
Displays a special effect on the entity."))

(cl-defmethod td-macs--name ((modifier td-macs-modifier-hit))
  "Returns the name of the modifier as a string."
  "Hit")

(cl-defmethod td-macs-print-string ((modifier td-macs-modifier-hit))
  (format "%s, Damage: %S" (cl-call-next-method modifier)
		  (td-macs-modifier-hit--dmg modifier)))

(defclass td-macs-modifier-poison (td-macs-modifier td-macs-highlight-mixin)
  ((dmg :initarg :dmg
		:initform 0
		:documentation "Is the total amount of damage that is done to the receiver.")
   (face :initform 'td-macs-entity-poison
		 :documentation "Is the face that gets used to draw the poison effect on the receiver"))
  (:documentation "Modifier for when a entity gets `hit`, e.g., it gets damaged.
Displays a special effect on the entity."))

(cl-defmethod td-macs--name ((modifier td-macs-modifier-poison))
  "Returns the name of the modifier as a string."
  "Poison")

(cl-defmethod td-macs-print-string ((modifier td-macs-modifier-poison))
  (with-slots (dmg total-duration) modifier
	  (format "%s, DPS: %S, Total: %d" (cl-call-next-method modifier)
			  (/ dmg (time-to-seconds total-duration)) dmg)))

(cl-defmethod td-macs--modifier-receiver-step ((modifier td-macs-modifier-poison))
  (with-slots (dmg total-duration receiver) modifier
	(with-slots (health) receiver
	  (setf health (- health 
					  (td-macs--calc-dmg (* dmg (min 1 (/ td-macs-tick-period (time-to-seconds total-duration))))
										 '(poison)
										 (td-macs-entity-resistances receiver)))))))

(defclass td-macs-modifier-speed (td-macs-modifier)
  ((speed-original :initarg :speed-original
				   :initform 0
				   :accessor td-macs-modifier-speed-speed-original
				   :documentation "Original speed of the receiver."))
  (:documentation "Modifier that deals with the speed of the receiver."))

(cl-defmethod td-macs--add ((modifier td-macs-modifier-speed) (entity td-macs-entity))
  "Cache the speed of ENTITY into SPEED-ORIGINAL of MODIFIER.  So
that we later can restore it."
  (cl-call-next-method modifier entity)
  (with-slots (speed-original) modifier
	(with-slots (speed) entity
	  (setf speed-original speed))))

(cl-defmethod td-macs--add ((entity td-macs-entity) (modifier td-macs-modifier-speed))
  "Adds the MODIFIER to the MODIFIERS slot of ENTITY.
Only when MODIFIERS does not contain a speed modifier.
When a speed modifier exists, it will be updated with MODIFIER."
  (with-slots (modifiers) entity
	(if-let ((speed-mod (seq-find (lambda (mod)
									(object-of-class-p mod (eieio-object-class modifier)))
								 modifiers)))
		(td-macs--add speed-mod modifier)
	  (cl-call-next-method entity modifier))))

(cl-defmethod td-macs--add ((original td-macs-modifier-speed) (addition td-macs-modifier-speed))
  "Adds the ADDITION to ORIGINAL.
This will update the slots of ORIGINAL.  This is done to avoid
having ordering and state problems in a modifiers slot of an
entity."
  (td-macs--modifier-ensure-total-duration original)
  (with-slots (duration total-duration) original
	(let ((addition-duration (td-macs-modifier-duration addition)))
	  (setf duration (time-add duration addition-duration)
			total-duration (time-add total-duration addition-duration)))))

(cl-defmethod td-macs--cleanup :after ((modifier td-macs-modifier-speed))
  "Restores the original speed of the RECEIVER of MODIFIER."
  (with-slots (speed) (td-macs-modifier-receiver modifier)
	(setf speed (td-macs-modifier-speed-speed-original modifier))))

(defclass td-macs-modifier-slow (td-macs-modifier-speed td-macs-highlight-mixin)
  ((slow :initarg :slow
		 :initform 0
		 :accessor td-macs-modifier-slow-slow
		 :documentation "Is the amount of speed that is slowed on the receiver.")
   (face :initform 'td-macs-entity-slow))
  (:documentation "Modifier that slows the speed of the receiver.  Till a max of
zero speed, i.e., standing still."))

(cl-defmethod td-macs--add :after ((modifier td-macs-modifier-slow) (entity td-macs-entity))
  "Reduces the SPEED of ENTITY with SLOW of MODIFIER.  Till we are
standing still (capped at 0)."
  (with-slots (speed-original slow) modifier
	(with-slots (speed) entity
	  (setf speed (max 0 (- speed slow))))))

(cl-defmethod td-macs--add :after ((original td-macs-modifier-slow) (addition td-macs-modifier-slow))
  "Accumulate the slow modifier of ORIGINAL and ADDITION into the
slow slot of ORIGINAL."
  (with-slots (slow) original
	(setf slow (+ slow (td-macs-modifier-slow-slow addition)))))

(cl-defmethod td-macs--name ((modifier td-macs-modifier-slow))
  "Returns the name of the modifier as a string."
  "Slow")

(defclass td-macs-modifier-haste (td-macs-modifier-speed td-macs-highlight-mixin)
  ((haste :initarg :haste
		 :initform 0
		 :accessor td-macs-modifier-haste-haste
		 :documentation "Is the amount of speed that is hastened on the receiver.")
   (face :initform 'td-macs-entity-haste))
  (:documentation "Modifier that speeds up (haste) the speed of the receiver."))

(cl-defmethod td-macs--name ((modifier td-macs-modifier-haste))
  "Returns the name of the modifier as a string."
  "Haste")

(cl-defmethod td-macs--add ((original td-macs-modifier-haste) (addition td-macs-modifier-haste))
  "Adds the ADDITION to ORIGINAL.
This will update the slots of ORIGINAL.  This will update the
SPEED of ORIGINAL to the maximum speed of ORIGINAL and ADDITION."
  (cl-call-next-method original addition)
  (with-slots (haste) original
	(setf haste (max haste (td-macs-modifier-haste-haste addition)))))

(cl-defmethod td-macs--add :after ((modifier td-macs-modifier-haste) (entity td-macs-entity))
  "We update the SPEED of ENTITY to the max of HASTE from already
existing speed modifier or current MODIFIER."
  (with-slots (speed-original haste) (or (seq-find 'td-macs-modifier-haste-p
												   (td-macs-entity-modifiers entity))
										 modifier)
	(with-slots (speed) entity
	  (setf speed (max (+ speed-original (td-macs-modifier-haste-haste modifier))
					   (+ speed-original haste))))))
;;; generic entities (for load order)
(cl-defstruct (td-macs-tower (:include td-macs-entity (code ?T)
									   (health 1)))
  (dmg '(0 . 0)
	   :documentation "CAR represents the dmg amount. 
 CDR represents the progress to the next fire/hit.")
  (dmg-types '(normal) :documentation "Damage types represented as a list of symbols.")
  (dmg-effects '() :documentation "List of effects, modifiers, that a hit will apply. ENTRY is the
postfix after td-macs-modifier-* struct/class.  The form is
'((ENTRY1 &rest ARGS-FOR-MAKE) (ENTRY2 &rest ARGS-FOR-MAKE))")
  (range '(0 0) :documentation "Area, in x and y coords, in which the tower can hit creeps")
  (level 0)
  (xp 0)
  (cost 0 :documentation "Amount of crystals that it takes to build this tower")
  (pos-upg '() :documentation "Association list representing the possible upgrades
 together with their cost. In the form of '((UPGRRADE1 . COST) (UPGRADE2 . COST))")
  (interest '(0 . 0)
			:documentation "CAR represents the interest amount. 
 CDR represents the progress to the next cash out."))

(cl-defstruct (td-macs-creep (:include td-macs-entity (code ?C)
									   (speed 1)
									   (resistances '((fire . -20)))
									   (health 1)))
  (path-pos 0 :documentation "Position in the creep path list.  That is used to check what
the next position of self is.")
  (crystals 1 :documentation "Number of crystals that the creep holds, and drops when destroyed"))

;;;; player entity
(cl-defstruct (td-macs-player (:include td-macs-entity (code ?P))))

(defun td-macs--player-code ()
  "Returns the default code for the player struct."
  (cadr (assoc 'code (cl-struct-slot-info 'td-macs-player))))

(cl-defmethod td-macs--draw :after ((entity td-macs-player))
  (when-let ((ent (td-macs--object-at-player-pos))
			 (towerp (td-macs-tower-p ent)))
	(td-macs--highlight-bounds (td-macs--entity-bounds ent 'td-macs-tower-range))))

;;;; tower entities
(defun td-macs--tower-interest (tower)
  "Returns the interest defined on TOWER.
Without the progress for cashing out."
  (car (td-macs-tower-interest tower)))

(defun td-macs--tower-upgrade-cost (tower upgrade)
  "Returns the cost that is needed for UPGRADE based on the TOWER."
  (if-let ((upg (cdr (assoc upgrade (td-macs-tower-pos-upg tower)))))
	  upg
	0))

(cl-defmethod td-macs-print-string ((entity td-macs-tower))
  (format "Tower, DMG: %S, Range: %S, Position: %S" (td-macs-tower-dmg entity)
		  (td-macs-tower-range entity) (td-macs-entity-pos entity)))

(cl-defmethod td-macs-print-string :around ((entity td-macs-tower))
  (concat (cl-call-next-method entity)
		  (when-let (dmg-effects (td-macs-tower-dmg-effects entity))
			(funcall 'concat "\n\tEffects:\n\t\t"
				   (mapconcat (lambda (effect) (capitalize
												(symbol-name (car effect))))
							  dmg-effects "\n\t\t")))))

(cl-defgeneric td-macs--tower-level-up-stats (tower))

(defun td-macs--tower-make (tower-fun position)
  "Makes a tower based on TOWER-FUN at POSITION.
It only does this if there are enough crystals in `td-macs-game-state'.
Otherwise it returns '()."
  (let ((tower (funcall tower-fun :pos (cons position 0))))
	(td-macs-with-plist (crystals) td-macs-game-state
	  (with-slots (cost) tower
		(if (>= crystals cost)
			(progn
			  (cl-decf crystals cost)
			  tower)
		  (message "Not enough crystals! You need %d crystals to build this tower." cost)
		  nil)))))

(defun td-macs--tower-level-up? (tower)
  "Exp calculations for now linear: 10 + 5x."
  (>= (td-macs-tower-xp tower) (+ 10 (* 5 (td-macs-tower-level tower)))))

(defun td-macs--tower-level-up (tower)
  (cl-incf (td-macs-tower-level tower))
  (td-macs--tower-level-up-stats tower))

(defun td-macs--tower-add-xp (tower added-xp)
  (cl-incf (td-macs-tower-xp tower) added-xp)
  (when (td-macs--tower-level-up? tower)
	  (td-macs--tower-level-up tower)))

(defun td-macs--calc-dmg (damage dmg-types resistances)
  (seq-reduce (lambda (dmg res) (- dmg (td-macs-*% dmg (cdr res))))
	(seq-remove 'null
	  (seq-map (lambda (dmg) (assoc dmg resistances))
			   dmg-types))
	damage))

(defun td-macs--tower-calc-dmg (tower resistances)
  (td-macs--calc-dmg (car (td-macs-tower-dmg tower))
					 (td-macs-tower-dmg-types tower)
					 resistances))

(defun td-macs--tower-hit (tower enemy)
  (let* ((damage (td-macs--tower-calc-dmg tower (td-macs-entity-resistances enemy)))
		(new-health (- (td-macs-entity-health enemy)
					   damage)))
	(setf (td-macs-entity-health enemy) new-health)
	(td-macs--add enemy
				  (list (make-instance 'td-macs-modifier-hit :duration '(0 1 0 0)
									   :dmg damage)))
	(td-macs--tower-apply-dmg-effects tower enemy)
	(when (<= new-health 0)
	  ;;rewrite this to destroy enemy, not just creep
	  (td-macs--destroy-creep enemy)
		  (td-macs--tower-add-xp tower (td-macs--creep-xp enemy)))))

(defun td-macs--dmg-effect-make (dmg-effect-spec)
  "Makes a dmg-effect modifier based on the DMG-EFFECT-SPEC."
  (apply 'make-instance
		 (intern (concat "td-macs-modifier-" (symbol-name (car dmg-effect-spec))))
		 (cdr dmg-effect-spec)))

(defun td-macs--tower-apply-dmg-effects (tower enemy)
  "Applies all the dmg effects that are registered on TOWER to ENEMY."
  (with-slots (dmg-effects) tower
	(td-macs--add enemy (seq-map 'td-macs--dmg-effect-make dmg-effects))))

(cl-defmethod td-macs--entity-step ((tower td-macs-tower))
  (td-macs-with-plist (creeps) td-macs-game-state
	(td-macs--tower-dmg-step tower creeps)
	(td-macs--tower-interest-step tower)))

(defun td-macs--tower-interest-step (tower)
  (when (= 1 (td-macs--incf-slot-by-speed tower td-macs-tower-interest 1))
			 (td-macs-with-plist (crystals) td-macs-game-state
			   (cl-incf crystals (td-macs--tower-interest tower)))))

(cl-defgeneric td-macs--tower-dmg-step (tower enemies)
  "Function that performs the damage calculations for the towers to
apply to the ENEMIES."
  (:method :around (tower enemies)
		   (when (= 1 (td-macs--incf-slot-by-speed tower td-macs-tower-dmg 1))
			 (cl-call-next-method tower enemies))))

(cl-defmethod td-macs--tower-dmg-step ((tower td-macs-tower) enemies)
  "Default damage method that deals single target damage to the ENEMIES."
  (when-let (enemy (seq-find (td-macs--entity-within-bounds-p
							  (td-macs--entity-bounds tower 'td-macs-tower-range))
							 enemies))
	(td-macs--tower-hit tower enemy)))

(cl-defstruct (td-macs-tower-spear (:include td-macs-tower (dmg '(1 . 0)) (range '(2 2))
											 (speed 2)
											 (code ?S)
											 (dmg-types '(piercing))
											 (pos-upg '((poison . 10) (fire . 10)))
											 (cost 1))))

(cl-defmethod td-macs--tower-level-up-stats ((tower td-macs-tower-spear))
  (with-slots (dmg speed) tower
	(setf dmg (cons (+ (car dmg) 0.01) (cdr dmg)))
   (cl-incf speed)))

(cl-defmethod td-macs-print-string ((entity td-macs-tower-spear))
  (format "Spear Tower, LV: %S (%S), DMG: %S, Range: %S, Position: %S"
		  (td-macs-tower-level entity) (td-macs-tower-xp entity)
		  (td-macs-tower-dmg entity)
		  (td-macs-tower-range entity) (td-macs-entity-pos entity)))

(cl-defstruct (td-macs-tower-dummy (:include td-macs-tower
											 (dmg '(0 . 0))
											 (pos-upg '((interest . 5)
														(increase-own-speed . 10))))))

(cl-defmethod td-macs-print-string ((entity td-macs-tower-dummy))
  (format "Useless Tower, DMG: %S, Range: %S, Position: %S, Speed: %S, Interest: %S" (td-macs-tower-dmg entity)
		  (td-macs-tower-range entity)
		  (td-macs-entity-pos entity)
		  (td-macs-entity-speed entity)
		  (td-macs-tower-interest entity)))

(cl-defstruct (td-macs-tower-bomb (:include td-macs-tower (dmg '(2 . 0)) (range '(2 2))
											(speed 1)
											(code ?B)
											(pos-upg '((slow . 15)))
											(cost 2)))
  (splash '(1 1)))

(cl-defmethod td-macs--tower-level-up-stats ((tower td-macs-tower-bomb))
  (with-slots (dmg splash) tower
	(setf dmg (cons (+ (car dmg) 1) (cdr dmg)))
   (setf splash (seq-mapn '+ splash '(0.5 0.5)))))

(cl-defmethod td-macs-print-string ((entity td-macs-tower-bomb))
  (format "Bomb Tower, LV: %S (%S), DMG: %S, Splash: %S, Range: %S, Position: %S"
		  (td-macs-tower-level entity) (td-macs-tower-xp entity) (td-macs-tower-dmg entity)
		  (td-macs-tower-bomb-splash entity) (td-macs-tower-range entity)
		  (td-macs-entity-pos entity)))

(cl-defmethod td-macs--tower-dmg-step ((tower td-macs-tower-bomb) creeps)
  "Damage method for BOMB Tower that deals splash damage."
  (when-let ((creep (seq-find (td-macs--entity-within-bounds-p
							   (td-macs--entity-bounds tower 'td-macs-tower-range))
							  creeps))
			 (surrounds (seq-filter (td-macs--entity-within-bounds-p
									 (list (td-macs--entity-pos creep)
										   (td-macs-tower-bomb-splash tower)))
									creeps)))
	(seq-do (lambda (creep) (td-macs--tower-hit tower creep)) surrounds)))

(cl-defstruct (td-macs-tower-buff (:include td-macs-tower (dmg '(0 . 0)) (range '(1 1))
											(speed 0.2)
											(code ?^)
											(pos-upg '((increase-own-speed . 20)
													   (speed . 15)))
											(cost 10))))

(cl-defmethod td-macs-print-string ((entity td-macs-tower-buff))
  (format "Buff Tower, LV: %S (%S), Speed: %S, Next Buff: %0.2f, Range: %S, Position: %S"
		  (td-macs-tower-level entity) (td-macs-tower-xp entity)
		  (td-macs-entity-speed entity)
		  (cdr (td-macs-tower-dmg entity)) (td-macs-tower-range entity)
		  (td-macs-entity-pos entity)))

(cl-defmethod td-macs--entity-step ((tower td-macs-tower-buff))
  "Entity step for TD-MACS-TOWER-BUFF that hits other towers instead
of creeps."
  (td-macs-with-plist (towers) td-macs-game-state
	(td-macs--tower-dmg-step tower (remove tower towers))
	(td-macs--tower-interest-step tower)))

(cl-defmethod td-macs--tower-dmg-step ((tower td-macs-tower-buff) enemies)
  "DMG step where the target to be hit is the enemy in ENEMIES with the least
modifiers."
  (td-macs-with-plist (towers) td-macs-game-state
	(when-let (enemy (car (seq-sort (lambda (tower-a tower-b)
									  (< (length (td-macs-entity-modifiers tower-a))
										 (length (td-macs-entity-modifiers tower-b))))
						   (seq-filter (td-macs--entity-within-bounds-p
										(td-macs--entity-bounds tower 'td-macs-tower-range))
									   enemies))))
	  (td-macs--tower-hit tower enemy))))

(cl-defmethod td-macs--tower-upgrade ((tower td-macs-tower-buff) (upgrade (eql 'speed)))
  "Adds the HASTE modifier to the DMG-EFFECTS on TOWER.
This will increase the speed of the other tower that has been
hit."
  (with-slots (dmg-effects) tower
	(setf dmg-effects (cons (list 'haste
								  :duration (seconds-to-time 1.5)
								  :haste 1)
							dmg-effects))))

(cl-defmethod td-macs--tower-upgrade ((tower td-macs-tower) (upgrade (eql 'poison)))
  "Adds the POISON modifier to the DMG-EFFECTS on TOWER.
This will make it that the entities being hit by TOWER, receive a
damage over time modifier in the form of POISON modifier."
  (with-slots (dmg-effects) tower
	(setf dmg-effects (cons (list 'poison
								  :duration (seconds-to-time 1)
								  :dmg 10)
							dmg-effects))))

(cl-defmethod td-macs--tower-upgrade ((tower td-macs-tower) (upgrade (eql 'fire)))
  "Adds the FIRE modifier to the MODIFIERS on TOWER.
This will make it that the TOWER temporarly shoots with the fire
dmg type."
  (with-slots (dmg-effects) tower
	(td-macs--add tower
				  (list (make-instance 'td-macs-modifier-fire :duration (seconds-to-time 10))))))

(defun td-macs--tower-destroy (tower &rest args)
  "Destroys, removes, the TOWER from the current game state."
  (td-macs-with-plist (towers) td-macs-game-state
	(setf towers (remove tower towers))))

(cl-defgeneric td-macs--tower-upgrade (tower upgrade))

(cl-defmethod td-macs--tower-upgrade :around ((tower td-macs-tower) (upgrade symbol))
  (td-macs-with-plist (crystals) td-macs-game-state
	(let ((cost (td-macs--tower-upgrade-cost tower upgrade)))
	  (if (>= crystals cost)
		  (progn (cl-decf crystals cost)
				 (cl-call-next-method tower upgrade))
		(message "Not enough crystals! You need %d crystals to upgrade the %s."
				 cost upgrade)))))

(cl-defmethod td-macs--tower-upgrade ((tower td-macs-tower) (upgrade (eql 'interest)))
  "Upgrades the interest on the TOWER.
In the future this may be differ between different types of TOWERS."
  (with-slots (interest) tower
	(setf interest (cons (+ 0.5 (td-macs--tower-interest tower))
						 (cdr interest)))))

(cl-defmethod td-macs--tower-upgrade ((tower td-macs-tower) (upgrade (eql 'slow)))
  "Adds the SLOW modifier to the DMG-EFFECTS on TOWER.
In the future this may be differ between different types of TOWERS."
  (with-slots (dmg-effects) tower
	;;these slow parameters are still a little to hard coded :)
	(setf dmg-effects (cons (list 'slow :duration (seconds-to-time 2)
								  :slow 3)
							dmg-effects))))

(cl-defmethod td-macs--tower-upgrade ((tower td-macs-tower) (upgrade (eql 'increase-own-speed)))
  "Adds the SLOW modifier to the modifiers list on TOWER.  With
negative slow so it increases the speed."
  (td-macs--add tower (list (make-instance 'td-macs-modifier-haste
										   :duration (seconds-to-time 60)
										   :haste 1))))

(cl-defmethod td-macs--tower-upgrade :after ((tower td-macs-tower-bomb) (upgrade (eql 'slow)))
  "Removes the slow UPGRADE from TOWER pos-upg slot."
  (with-slots (pos-upg) tower
	;;these slow parameters are still a little to hard coded :)
	(setf pos-upg (remove (assoc upgrade pos-upg) pos-upg))))

(defun td-macs-make-action-alist-string (alist -prefix)
  "Makes a string for a action-alist in the form of \"(choice-char) action\"."
  (let ((len (length -prefix)))
	(mapconcat
	 (lambda (action)
	   (format "(%s) %s"
			   (propertize (char-to-string (car action)) 'face 'highlight)
			   (if (consp (cdr action))
				   (symbol-name (cddr action))
				 (substring (symbol-name (cdr action)) len))))
	 alist ", ")))

(defvar td-macs-tower-insert-action-alist
  '((?s . make-td-macs-tower-spear)
	(?b . make-td-macs-tower-bomb)
	(?^ . make-td-macs-tower-buff)
	(?d . make-td-macs-tower-dummy)))

(defun td-macs-tower-insert-dispatch-help-string ()
  (td-macs-make-action-alist-string td-macs-tower-insert-action-alist
									"make-td-macs-tower-"))

;; For now this is too simple, maybe dispatch over tower
(defvar td-macs-tower-default-menu-action-alist
  (cons ?x  (cons 'td-macs--tower-destroy 'destroy)))

(defun td-macs--tower-build-menu-entry (upg-with-cost prefix)
  "Builds the entry for 1 upgrade as defined by UPG-WITH-COST and PREFIX."
  (cons prefix (cons 'td-macs--tower-upgrade (car upg-with-cost))))

(defun td-macs--tower-menu-action-alist (tower)
  "Builds up the ALIST based on TOWER which from the player can choose for the upgrades in the menu."
  ; maybe the prexifes in a var instead of hardcoded
  (cons td-macs-tower-default-menu-action-alist
		(seq-mapn 'td-macs--tower-build-menu-entry
				  (td-macs-tower-pos-upg tower) '(?q ?w ?e ?r ?t ?y ?u ?i ?o ?p))))


(defun td-macs-tower-menu-dispatch-help-string (tower)
  (td-macs-make-action-alist-string (td-macs--tower-menu-action-alist tower)
									"td-macs--tower-"))

;;;; creep entities
(cl-defmethod td-macs-print-string ((entity td-macs-creep))
  (format "Creep, Health: %d, Position: %S" (td-macs-entity-health entity)
		  (td-macs-entity-pos entity)))

(cl-defstruct (td-macs-creep-runner (:include td-macs-creep
											  (speed 6)
											  (resistances '((piercing . 5)))))) ;for now we dont use a different creep-code

(cl-defmethod td-macs-print-string ((entity td-macs-creep-runner))
  (format "Runner, Health: %d, Position: %S" (td-macs-entity-health entity)
		  (td-macs-entity-pos entity)))

(cl-defmethod td-macs--entity-step ((entity td-macs-creep))
  (td-macs-creep-hit-fortress entity)
  (td-macs--creep-move entity))

(defun td-macs--creep-xp (creep)
  "For now we give back 1 xp. But this can be different based on creep"
  1)

(defun td-macs--creep-generator (amount constructor)
  (lambda ()
	(cons amount
		  (lambda (&rest args)
			(let ((creep (apply constructor args)))
			  (td-macs-with-plist (waves-survived) td-macs-game-state
				(with-slots (health) creep
				  (cl-incf health (+ health waves-survived)))
				creep))))))


;;; Functionality
(defclass td-macs-modifier-fire (td-macs-modifier)
  ((faces :initarg :faces
				   :initform '(td-macs-modifier-fire-one td-macs-modifier-fire-two td-macs-modifier-fire-three)
				   :accessor td-macs-modifier-fire-faces
				   :documentation "This is a list of faces that are used for drawing.")
   (draw-progress :initform 0
				  :accessor td-macs-modifier-fire-draw-progress
				  :documentation "Progress counter that keeps track of when to draw a new face of the FACES slot."))
  (:documentation "Modifier that applies a dmg effect to the receiver"))

(cl-defmethod td-macs--name ((modifier td-macs-modifier-fire))
  "Returns the name of the modifier as a string."
  "Fire")

(cl-defmethod td-macs--add :after ((modifier td-macs-modifier-fire) (tower td-macs-tower))
  "When MODIFIER is added to the TOWER it adds the FIRE dmg type on
the DMG-TYPES slot of TOWER."
  (with-slots (dmg-types) tower
	(setf dmg-types (cons 'fire dmg-types))))

(cl-defmethod td-macs--cleanup :after ((modifier td-macs-modifier-fire))
  "Removes the FIRE dmg type from the DMG-TYPES slot of RECEIVER of
MODIFIER."
  (let ((receiver (td-macs-modifier-receiver modifier)))
	(with-slots (dmg-types) receiver
	  (setf dmg-types (seq-difference dmg-types '(fire))))))

(cl-defmethod td-macs--draw ((modifier td-macs-modifier-fire))
  "Highlight the RECEIVER of MODIFIER based on the faces slot."
  (with-slots (faces receiver draw-progress) modifier
	(setf draw-progress (+ draw-progress td-macs-tick-period))
	(let ((face (car faces)))
	  (when (>= draw-progress 0.5)
		(setf draw-progress 0)
		(setf faces (append (cdr faces) (list face))))
	  (td-macs--entity-apply-face receiver face))))

(provide 'td-macs-entities)

;;; td-macs-entities.el ends here
