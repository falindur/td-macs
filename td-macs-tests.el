;;; package --- td-macs-tests  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(mapcar #'require '(gamegrid map seq cl-macs td-macs-base
							 td-macs-entities td-macs))

(defmacro td-macs-with-temp-game-buffer (&rest body)
  `(let ((td-macs-buffer-name " *temp-td-macs*")
		 (td-macs-stats-buffer-name " *temp-td-macs-stats*")
		 ;; we don't use glyphs atm
		 (td-macs-use-glyphs-flag nil))
	 (unwind-protect
		 (save-excursion
		   (with-current-buffer (get-buffer-create td-macs-buffer-name)
			 ,@body))
	   (kill-buffer td-macs-buffer-name)
	   (kill-buffer td-macs-stats-buffer-name))))

(ert-deftest td-macs-with-plist-get-test ()
  (let ((plist (list :hoi 5 :doei 6)))
	(td-macs-with-plist (hoi doei) plist
	  (should (equal hoi (plist-get plist :hoi)))
	  (should (equal doei (plist-get plist :doei))))))

(ert-deftest td-macs-with-plist-change-test ()
  (let ((plist (list :hoi 5 :doei 6)))
	(td-macs-with-plist (hoi doei) plist
	  (should (= 5 (plist-get plist :hoi)))
	  (setf hoi "doei")
	  (cl-incf doei)
	  (should (and (equal (plist-get plist :hoi) "doei")
				   (equal (plist-get plist :doei) 7))))))

(ert-deftest td-macs-*%-situations ()
  "Tests different situations for where -*% should work."
  (should (= 0 (td-macs-*% 5 0)))
  (should (= 4 (td-macs-*% 5 80)))
  (should (= 10 (td-macs-*% 5 200)))
  (should (= -1 (td-macs-*% 5 -20)))
  (should (= 0 (td-macs-*% -5 0)))
  (should (= -4 (td-macs-*% -5 80))))

(ert-deftest td-macs-tower-insert-dummy ()
  "Tower insert when player is at ground location"
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :player (make-td-macs-player :pos (cons '(0 1) 0))))))
	(td-macs-tower-dispatch-menu ?d)
	(should (td-macs-tower-dummy-p (car (plist-get td-macs-game-state :towers))))))

(ert-deftest td-macs-tower-insert-dummy-fail ()
  "Tower insert fail when player is not at a ground location"
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :level '((2)) :player (make-td-macs-player :pos (cons '(0 0) 0))))))
	(td-macs-tower-dispatch-menu ?d)
	(should (null (plist-get td-macs-game-state :towers)))))

(ert-deftest td-macs-tower-menu-popup ()
  "Tower insert popups tower menu is at a tower location"
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :level '((0))
											 :towers (list (make-td-macs-tower-dummy :pos (cons '(0 0) 0)))
											 :player (make-td-macs-player :pos (cons '(0 0) 0))))))
	(setq inhibit-interaction t)
	(should-error (td-macs-tower-dispatch-menu)
				  :type 'inhibited-interaction)
	(setq inhibit-interaction nil)))

(ert-deftest td-macs-tower-destroy ()
  "Tower destroy when at a tower location"
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :level '((0))
											 :towers (list (make-td-macs-tower-dummy :pos (cons '(0 0) 0)))
											 :player (make-td-macs-player :pos (cons '(0 0) 0))))))
	(td-macs-with-plist (towers) td-macs-game-state
	  (should (not (null towers)))
	  (execute-kbd-macro (kbd "M-x td-macs-tower-dispatch-menu <RET> x"))
	  (should (null towers)))))

(ert-deftest td-macs-destroy-creep-get-crystals ()
  "When a creep gets destroyed we should increase crystals"
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :creeps (list (make-td-macs-creep :crystals 2))
											 :crystals 3))))
	(td-macs-with-plist (creeps crystals) td-macs-game-state
	  (should (= 3 crystals))
	  (td-macs--destroy-creep (car creeps))
	  (should (null creeps))
	  (should (= 5 crystals)))))

(ert-deftest td-macs-level-specifies-start-crystal-amount ()
  "The level that is selected should specify the starting crystal amount"
  (let ((td-macs-playable-levels '(("test-level" .
									(:waves td-macs-default-creep-waves
											:level td-macs-default-map
											:crystals 5)))))
	(td-macs-with-temp-game-buffer
	 (td-macs-start-session "test-level")
	 (should (= (plist-get td-macs-game-state :crystals) 5))
	 (td-macs-quit-window))))

(ert-deftest td-macs-spend-crystals-by-inserting-tower ()
  "When inserting a tower should cost crystals"
  (let* ((cost (td-macs-tower-cost (make-td-macs-tower-spear)))
		 (td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
										(list :player (make-td-macs-player :pos (cons '(0 1) 0))
											  :crystals cost))))
	(should (= cost (plist-get td-macs-game-state :crystals)))
	(td-macs-tower-dispatch-menu ?s)
	(should (zerop (plist-get td-macs-game-state :crystals)))))

(ert-deftest td-macs-spend-crystals-by-inserting-tower-failed ()
  "When inserting a tower should cost crystals, and when not enough fails inserting a tower."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :player (make-td-macs-player :pos (cons '(0 1) 0))
											 :crystals 0))))
	(td-macs-tower-dispatch-menu ?s)
	(should (null (plist-get td-macs-game-state :towers)))))

(ert-deftest td-macs-by-default-no-interest-on-towers ()
  "By default towers should not give interest on crystals."
  (let ((td-macs-tick-period 1)
		(td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :towers (list (make-td-macs-tower-dummy))
											 :crystals 5))))
	(dotimes (index 500)
	  (td-macs-towers-step)
	  (should (= 5 (plist-get td-macs-game-state :crystals))))))

(ert-deftest td-macs-gaining-interest-on-towers ()
  "When there is interest on towers than we expect to have more after time."
  (let ((td-macs-tick-period 1)
		(td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :towers (list (make-td-macs-tower-dummy :interest '(2 . 0) :speed 1))
											 :crystals 5))))
	(dotimes (index 50)
	  (td-macs-towers-step)
	  (should (= (+ 5 (* 2 (1+ index))) (plist-get td-macs-game-state :crystals))))))

(ert-deftest td-macs-no-dmg-on-creep-when-no-speed ()
  "When there is no speed on the tower we don't expect any damage dealt."
  (let ((td-macs-tick-period 1)
		(td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :level '((0 0))
											 :towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																					 :speed 0))
											 :creeps (list (make-td-macs-creep :health 10 :pos (cons (list 1 0) 0)))))))
	(dotimes (index 500)
	  (td-macs-towers-step)
	  (should (= 10 (td-macs-entity-health (car (plist-get td-macs-game-state :creeps))))))))

(ert-deftest td-macs-dmg-on-creep-when-there-is-speed ()
  "When there is speed on the tower we expect damage over time."
  (let ((td-macs-tick-period 1)
		(td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :level '((0 0))
											 :towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																					 :range '(1 1)
																					 :speed 1))
											 :creeps (list (make-td-macs-creep :health 10 :pos (cons (list 1 0) 0)))))))
	(dotimes (index 9)
	  (td-macs-towers-step)
	  (should (= (- 10 (1+ index)) (td-macs-entity-health (car (plist-get td-macs-game-state :creeps))))))))

(ert-deftest td-macs-upgrade-interest-on-towers ()
  "When we upgrade the interest it should become higher."
  (let ((tower (make-td-macs-tower-dummy :pos-upg '((interest . 0))))
		(td-macs-game-state (list :crystals 2)))
	(should (zerop (td-macs--tower-interest tower)))
	(td-macs--tower-upgrade tower 'interest)
	(should (cl-plusp (td-macs--tower-interest tower)))))

(ert-deftest td-macs-upgrade-interest-on-towers-fails-when-cost-too-high ()
  "When the cost is too high we can't upgrade."
  (let ((tower (make-td-macs-tower-dummy :pos-upg '((interest . 5))))
		(td-macs-game-state (list :crystals 2)))
	(should (= 2 (plist-get td-macs-game-state :crystals)))
	(should (zerop (td-macs--tower-interest tower)))
	(td-macs--tower-upgrade tower 'interest)
	(should (zerop (td-macs--tower-interest tower)))
	(should (= 2 (plist-get td-macs-game-state :crystals)))))

(ert-deftest td-macs-leveling-up-should-not-update-default-slot-values ()
  "Increasing stats because of a level up should not increase default slot values."
  (let ((before-slots (copy-tree (cl-struct-slot-info 'td-macs-tower-spear)))
		(tower (make-td-macs-tower-spear)))
	(td-macs--tower-level-up tower)
	(should (equal before-slots (cl-struct-slot-info 'td-macs-tower-spear)))))

(ert-deftest td-macs-tower-upgrade-slot-determines-dispatch-list ()
  "The tower know itself which upgrades the user can choose from."
  (let ((tower (make-td-macs-tower :pos-upg '((test . 0)))))
	(should (equal (list (cons ?x (cons 'td-macs--tower-destroy 'destroy)) ;; always exits
						 (cons ?q (cons 'td-macs--tower-upgrade 'test)))
				   (td-macs--tower-menu-action-alist tower)))
	;; change pos-upg
	(setf (td-macs-tower-pos-upg tower) (list (cons 'changed '(5))))
	(should (equal (list (cons ?x (cons 'td-macs--tower-destroy 'destroy)) ;; always exits
						 (cons ?q (cons 'td-macs--tower-upgrade 'changed)))
				   (td-macs--tower-menu-action-alist tower)))))

(ert-deftest td-macs-waves-are-not-endless ()
  "When we don't specify a endlessnes the waves stop when we run out."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :waves (list (cons td-macs-default-creeps-amount 'make-td-macs-creep))))))
	(should (null (td-macs--wave-swap-new-wave)))))

(ert-deftest td-macs-waves-are-endless ()
  "When we specify a endlessnes in the waves we continue till we lost the game or forever."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :waves (list (cons td-macs-default-creeps-amount 'make-td-macs-creep)
														  (lambda () (cons td-macs-default-creeps-amount 'make-td-macs-creep)))))))
	(dotimes (index 100)
	  (should (not (null (td-macs--wave-swap-new-wave)))))))

(ert-deftest td-macs-waves-endless-creeps-get-more-difficult-each-wave ()
  "Based on the generator used the creeps get more difficult, e.g., more health or high resistance."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :waves (list (cons td-macs-default-creeps-amount 'make-td-macs-creep)
														  (td-macs--creep-generator 15 'make-td-macs-creep))))))
	(td-macs--wave-swap-new-wave)
	(let ((test-health 0))
	  (dotimes (index 100)
		(td-macs--wave-swap-new-wave)
		(let ((creep (funcall (cdr (plist-get td-macs-game-state :current-wave)))))
		  (should (< test-health (td-macs-entity-health creep)))
		  (setq test-health (td-macs-entity-health creep)))))))

(ert-deftest td-macs-spawn-creep-without-affecting-game-state ()
  "It should be possible to spawn a creep without affecting game state."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
										;so we dont stand on spawn
									   (list :player (make-td-macs-player :pos (cons '(0 1) 0))
											 :current-wave (cons 10  'make-td-macs-creep)
											 :creeps-amount 2))))
	(should (td-macs-creep-p (td-macs--creep-spawn)))
	(should (null (plist-get td-macs-game-state :creeps)))))

(ert-deftest td-macs-creep-having-resistance-reduces-dmg ()
  "Having resistance reduces the damage dealt by the tower"
  (let ((tower (make-td-macs-tower :dmg '(4 . 0) :dmg-types '(test-flame)))
		(creep (make-td-macs-creep :health 5 :resistances '((test-flame . 75)))))
	(td-macs--tower-hit tower creep) ;because of resistance we take only 1 dmg (75% reduction)
	(should (= 4 (td-macs-entity-health creep)))))

(ert-deftest td-macs-creep-having-wrong-resistance-does-not-reduces-dmg ()
  "Having the wrong resistance does not reduce the damage dealt by the tower."
  (let ((tower (make-td-macs-tower :dmg '(4 . 0) :dmg-types '(test-normal)))
		(creep (make-td-macs-creep :health 5 :resistances '((test-flame . 75)))))
	(td-macs--tower-hit tower creep) ;because of no resistance we take 4 dmg (0% reduction)
	(should (= 1 (td-macs-entity-health creep)))))

(ert-deftest td-macs-creep-dies-after-hitting-fortress ()
  "Creep should despawn when it hits the posiion of the fortress."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
										;so we dont stand on spawn
									   (list :level `((,td-macs-path-code ,td-macs-fortress-code))
											 :fortress (list 1 0)
											 :creep-path '((1 0) (0 0))
											 :creeps (list (make-td-macs-creep :pos (cons (list 0 0) 0)))))))
	(td-macs--entity-step (car (plist-get td-macs-game-state :creeps)))
	(should (not (null (plist-get td-macs-game-state :creeps))))

	(setf (td-macs-entity-pos (car (plist-get td-macs-game-state :creeps))) (cons (list 1 0) 0))

	(td-macs--entity-step (car (plist-get td-macs-game-state :creeps)))
	(should (null (plist-get td-macs-game-state :creeps)))))

(ert-deftest td-macs-health-decreases-after-creep-hits-fortress ()
  "The health should decrease after creep has hit the fortress."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
										;so we dont stand on spawn
									   (list :level `((,td-macs-path-code ,td-macs-fortress-code))
											 :fortress (list 1 0)
											 :health 1
											 :creep-path '((1 0) (0 0))
											 :creeps (list (make-td-macs-creep :pos (cons (list 0 0) 0)))))))
	(td-macs--entity-step (car (plist-get td-macs-game-state :creeps)))
	(should (= 1 (plist-get td-macs-game-state :health)))

	(setf (td-macs-entity-pos (car (plist-get td-macs-game-state :creeps))) (cons (list 1 0) 0))

	(td-macs--entity-step (car (plist-get td-macs-game-state :creeps)))
	(should (< (plist-get td-macs-game-state :health) 1))))

(ert-deftest td-macs-highlight-background-nearby-spaces-when-on-tower ()
  "Highlight the background of the spaces around a tower, the range, when the player is on the tower."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-ground-code ,td-macs-path-code ,td-macs-path-code)
										 (,td-macs-ground-code ,td-macs-ground-code ,td-macs-path-code)
										 (,td-macs-ground-code ,td-macs-path-code ,td-macs-path-code)
										 (,td-macs-ground-code ,td-macs-path-code ,td-macs-ground-code))
								:towers (list (make-td-macs-tower-dummy :range (list 1 1)
																		:pos (cons (list 1 1) 0)))
								:player (make-td-macs-player :pos (cons (list 0 0) 0))
								:creep-path '((1 0) (0 0))
								))))
	(td-macs-with-temp-game-buffer	 
	 (td-macs--start-session-from-state state)
	 (should (and (= 0 (td-macs-pos-x (td-macs--entity-pos (plist-get td-macs-game-state :player))))
				  (= 0 (td-macs-pos-y (td-macs--entity-pos (plist-get td-macs-game-state :player))))))
	 
	 ;; The nils represen the newlines and end of buffer
	 (should (equal (list 'nil 'nil 'nil nil
						  'nil 'nil 'nil nil
						  'nil 'nil 'nil nil
						  'nil 'nil 'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max))))))

	 (setf (td-macs-entity-pos (plist-get td-macs-game-state :player)) (cons (list 1 1) 0))
	 (td-macs--draw (plist-get td-macs-game-state :player))

	 ;; the last row is not highlighted because range is 1x1
	 (should (equal (list 'td-macs-highlight-entity 'td-macs-highlight-entity 'td-macs-highlight-entity nil
						  'td-macs-highlight-entity 'td-macs-highlight-entity 'td-macs-highlight-entity nil
						  'td-macs-highlight-entity 'td-macs-highlight-entity 'td-macs-highlight-entity nil
						  'nil 'nil 'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max)))))))))

(ert-deftest td-macs-highlight-background-when-on-tower-should-not-highlight-newlines ()
  "Highlight the background of the spaces around a tower.  But
should not highlight the new lines that are out of level bounds."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-ground-code ,td-macs-path-code ,td-macs-ground-code)
										 (,td-macs-ground-code ,td-macs-path-code ,td-macs-ground-code)
										 (,td-macs-ground-code ,td-macs-path-code ,td-macs-ground-code)
										 (,td-macs-ground-code ,td-macs-path-code ,td-macs-ground-code))
								:towers (list (make-td-macs-tower-dummy :range (list 1 1)
																		:pos (cons (list 2 1) 0)))
								:player (make-td-macs-player :pos (cons (list 0 0) 0))
								:creep-path '((1 0) (0 0))
								))))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)
	 (should (and (= 0 (td-macs-pos-x (td-macs--entity-pos (plist-get td-macs-game-state :player))))
				  (= 0 (td-macs-pos-y (td-macs--entity-pos (plist-get td-macs-game-state :player))))))
	 
	 ;; The nils represen the newlines and end of buffer
	 (should (equal (list 'nil 'nil 'nil nil
						  'nil 'nil 'nil nil
						  'nil 'nil 'nil nil
						  'nil 'nil 'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max))))))

	 (setf (td-macs-entity-pos (plist-get td-macs-game-state :player)) (cons (list 2 1) 0))
	 (td-macs--draw (plist-get td-macs-game-state :player))

	 ;; the last row is not highlighted because range is 1x1
	 (should (equal (list 'nil 'td-macs-highlight-entity 'td-macs-highlight-entity nil
						  'nil 'td-macs-highlight-entity 'td-macs-highlight-entity nil
						  'nil 'td-macs-highlight-entity 'td-macs-highlight-entity nil
						  'nil 'nil 'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max)))))))))

(ert-deftest td-macs-highlight-background-when-on-tower-should-not-highlight-newlines-backwards ()
  "Highlight the background of the spaces around a tower.  But
should not highlight the new lines that are out of level bounds.  Also the newlines from last line, negative x-bounds."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-ground-code ,td-macs-path-code ,td-macs-ground-code)
										 (,td-macs-ground-code ,td-macs-path-code ,td-macs-ground-code)
										 (,td-macs-ground-code ,td-macs-path-code ,td-macs-ground-code)
										 (,td-macs-ground-code ,td-macs-path-code ,td-macs-ground-code))
								:towers (list (make-td-macs-tower-dummy :range (list 1 1)
																		:pos (cons (list 0 1) 0)))
								:player (make-td-macs-player :pos (cons (list 0 0) 0))
								:creep-path '((1 0) (0 0))
								))))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)
	 (should (and (= 0 (td-macs-pos-x (td-macs--entity-pos (plist-get td-macs-game-state :player))))
				  (= 0 (td-macs-pos-y (td-macs--entity-pos (plist-get td-macs-game-state :player))))))
	 
	 ;; The nils represen the newlines and end of buffer
	 (should (equal (list 'nil 'nil 'nil nil
						  'nil 'nil 'nil nil
						  'nil 'nil 'nil nil
						  'nil 'nil 'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max))))))

	 (setf (td-macs-entity-pos (plist-get td-macs-game-state :player)) (cons (list 0 1) 0))
	 (td-macs--draw (plist-get td-macs-game-state :player))

	 ;; the last row is not highlighted because range is 1x1
	 (should (equal (list 'td-macs-highlight-entity 'td-macs-highlight-entity 'nil nil
						  'td-macs-highlight-entity 'td-macs-highlight-entity 'nil nil
						  'td-macs-highlight-entity 'td-macs-highlight-entity 'nil nil
						  'nil 'nil 'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max)))))))))

(ert-deftest td-macs-range-positive-numbers ()
  (should (equal (list 1 2 3 4 5 6 7 8 9 10)
				 (td-macs-range 1 10))))

(ert-deftest td-macs-range-negative-to-positive-numbers ()
  (should (equal (list -8 -7 -6 -5 -4 -3 -2 -1 0 1 2)
				 (td-macs-range -8 2))))

(ert-deftest td-macs-range-negative-to-negative-numbers ()
  (should (equal (list -8 -7 -6 -5 -4)
				 (td-macs-range -8 -4))))

(ert-deftest td-macs-range-start-larger-then-end ()
  "The resulting range should always go from the lowest number to the highest number."
  (should (equal (list 5 6 7 8 9 10)
				 (td-macs-range 10 5))))

(ert-deftest td-macs-player-can-not-pass-bounds-of-level ()
  "The player is not able to move further than the bounds, in x and y direction, of the level."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :level `((,td-macs-ground-code ,td-macs-path-code)
													  (,td-macs-ground-code ,td-macs-path-code))
											 :player (make-td-macs-player :pos (cons (list 0 0) 0))))))
	(should (equal '(0 0) (td-macs--entity-pos (plist-get td-macs-game-state :player))))

	(td-macs-player-move-left)
	(should (equal '(0 0) (td-macs--entity-pos (plist-get td-macs-game-state :player))))

	(td-macs-player-move-down)
	(should (equal '(0 1) (td-macs--entity-pos (plist-get td-macs-game-state :player))))

	(td-macs-player-move-down)
	(should (equal '(0 1) (td-macs--entity-pos (plist-get td-macs-game-state :player))))

	(td-macs-player-move-right)
	(should (equal '(1 1) (td-macs--entity-pos (plist-get td-macs-game-state :player))))

	(td-macs-player-move-right)
	(should (equal '(1 1) (td-macs--entity-pos (plist-get td-macs-game-state :player))))))

(ert-deftest td-macs-modifier-duration-has-an-end ()
  "Check that a modifier has only a specific duration."
  (let ((modifier (make-instance 'td-macs-modifier :duration '(0 0 20000 0) ;;2ms
										 ))
		;; To simulate time
		(td-macs-tick-period 0.011))
	(should (null (td-macs--modifier-duration-over-p modifier)))
	(td-macs--modifier-step modifier)
	(should (null (td-macs--modifier-duration-over-p modifier)))
	(td-macs--modifier-step modifier)
	(should (td-macs--modifier-duration-over-p modifier))))

(ert-deftest td-macs-pausing-the-game-should-not-reduce-modifier-duration ()
  "When a game gets paused the modifier duration should not be reduced."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-ground-code ,td-macs-ground-code))
								:spawn (list 0 0)
								:creep-path (list '(0 0) '(0 0))
								:creeps (list (make-td-macs-creep :pos (cons (list 0 0) 0))))))
		;; To simulate time
		(td-macs-tick-period 0.05))
	(td-macs-with-temp-game-buffer
	 (td-macs-with-plist (creeps) td-macs-game-state
	   (td-macs--start-session-from-state state)
	   (td-macs--add (car creeps)
					 (list (make-instance 'td-macs-modifier :duration (seconds-to-time (* 2 td-macs-tick-period)))))
	  (should (not (null (td-macs-entity-modifiers (car creeps)))))

	  (sleep-for 0.11) ;;sleep should not affect ticks
	  (td-macs--entity-step (car creeps))
	  (should (not (null (td-macs-entity-modifiers (car creeps)))))

	  (let ((td-macs-paused t)) ;; Pause the game
		(dotimes (steps 500)	  ;;simulate 500 steps/ticks
		  (td-macs-game-step (current-buffer))))
	  (td-macs-game-step (current-buffer)) ;; One step so we forwarded 0.16 in total game time, less then duration of modifier
	  (should (not (null (td-macs-entity-modifiers (car creeps)))))))))

(ert-deftest td-macs-modifier-adding-and-removing-to-entities ()
  "Modifiers can be added to an entity and automatically get removed from it after duration is over."
  (let ((entity (make-td-macs-entity)))
	(should (null (td-macs-entity-modifiers entity)))
	(td-macs--add entity (list (make-instance 'td-macs-modifier :duration (seconds-to-time 0.40) ;;40ms
													  )))
	(should (not (null (td-macs-entity-modifiers entity))))
	(let ((td-macs-tick-period 0.2))
	  (td-macs--entity-step entity)
	  (should (not (null (td-macs-entity-modifiers entity))))
	  (td-macs--entity-step entity)
	  (should (not (null (td-macs-entity-modifiers entity))))
	  (td-macs--entity-step entity)
	  (should (null (td-macs-entity-modifiers entity))))))

(ert-deftest td-macs-modifier-changes-receiver ()
  "Modifier changes the receiver when we step over it."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-path-code ,td-macs-path-code))
								:creeps (list (make-td-macs-creep :pos (cons (list 0 0) 0)))
								:creep-path (list '(0 0) '(0 0))
								:player (make-td-macs-player :pos (cons (list 1 0) 0))))))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)	

	 (should (equal (list 'nil 'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max))))))

	 (td-macs--add (car (plist-get td-macs-game-state :creeps))
				   (list (make-instance 'td-macs-modifier-hit :duration '(1 0 0 0))))
	 (td-macs--entity-step (car (plist-get td-macs-game-state :creeps)))
	 (td-macs-game-draw td-macs-game-state)
	 (should (equal (list 'td-macs-entity-hit 'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max)))))))))

(ert-deftest td-macs-tower-hits-creep-changes-creep-graphic ()
  "When the tower hits a creep the graphic changes to show it was hit."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-ground-code ,td-macs-ground-code))
								:creeps (list (make-td-macs-creep :pos (cons (list 0 0) 0)
																  :health 5))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0)
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:speed 1
																		:pos (cons (list 1 0) 0)))
								:player (make-td-macs-player :pos (cons (list 2 0) 0)))))
		(td-macs-tick-period 1))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)	
	 (should (equal (list 'nil 'nil 'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max))))))
	 (td-macs-creeps-step)
	 (td-macs-towers-step)
	 (td-macs-game-draw td-macs-game-state)
	 (should (equal (list 'td-macs-entity-hit 'nil
						  'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max)))))))))

(ert-deftest td-macs-modifier-shows-on-the-stats-buffer ()
  "Modifiers of the current selected object are shown on the stats
buffer.  Atleast when the player is on the object."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-path-code))
								:creeps (list (make-td-macs-creep :pos (cons (list 0 0) 0)))
								:player (make-td-macs-player :pos (cons (list 0 0) 0)))))
		(modifier (make-instance 'td-macs-modifier :duration '(1 0 0 0))))
	(td-macs-with-temp-game-buffer
	  (td-macs--start-session-from-state state)
	  (save-excursion
		(with-current-buffer td-macs-stats-buffer-name
		  (goto-char (point-min))
		  (should (not (search-forward (td-macs-print-string modifier) nil t)))))

	  (td-macs--add (car (plist-get td-macs-game-state :creeps))
					(list modifier))
	  (td-macs-game-draw td-macs-game-state)

	  (save-excursion
		(with-current-buffer td-macs-stats-buffer-name
		  (goto-char (point-min))
		  (should (search-forward (td-macs-print-string modifier))))))))

(ert-deftest td-macs-modifier-poison-doing-dmg-over-time ()
  "Poison modifier does damage over time when it gets added to a creep."
  (let* ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0))))
		 (creep (make-td-macs-creep :health 5))
		 (td-macs-tick-period 0.2)
		 (health (td-macs-entity-health creep)))
	(td-macs--add creep (list (make-instance 'td-macs-modifier-poison :duration (seconds-to-time 0.40)
											 :dmg 4)))
	(td-macs--entity-step creep)
	(should (< (td-macs-entity-health creep) health))
	(setq health (td-macs-entity-health creep))

	(td-macs--entity-step creep)
	(should (< (td-macs-entity-health creep) health))
	(setq health (td-macs-entity-health creep))

	;;health should stay the same because duration is over
	(td-macs--entity-step creep)
	(should (= (td-macs-entity-health creep) health))))

(ert-deftest td-macs-modifier-poison-doing-total-dmg ()
  "Poison modifier does damage over time and in the end it has done
the total amount of damage specified.  Not more or less."
  (let* ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0))))
		 (creep (make-td-macs-creep :health 5))
		 (td-macs-tick-period 0.2))
	(td-macs--add creep (list (make-instance 'td-macs-modifier-poison :duration (seconds-to-time 0.10)
											 :dmg 4)))
	(td-macs--entity-step creep)
	(td-macs--entity-step creep)
	(should (= (td-macs-entity-health creep) 1))))

(ert-deftest td-macs-modifier-poison-doing-less-damage-when-creep-has-resistance-to-it ()
  "Poison modifier does damage over time on creep but does %-wise less damage when creep resistance to poison."
  (let* ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0))))
		 (creep (make-td-macs-creep :health 5 :resistances '((poison . 50))))
		 (td-macs-tick-period 0.2))
	(td-macs--add creep (list (make-instance 'td-macs-modifier-poison :duration (seconds-to-time 0.40)
											 :dmg 4)))
	(td-macs--entity-step creep)
	(should (= (td-macs-entity-health creep) 4)) ;; dmg 50% reduced by resistance so 1 instead of 2

	(td-macs--entity-step creep)
	(should (= (td-macs-entity-health creep) 3))

	;;health should stay the same because duration is over
	(td-macs--entity-step creep)
	(should (= (td-macs-entity-health creep) 3))))

(ert-deftest td-macs-modifier-slow-slows-speed-and-returns-speed-back-after-duration-is-over ()
  "When the slow modifier has been added it will slow the speed of the entity."
  (let* ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0))))
		 (creep (make-td-macs-creep :speed 2))
		 (td-macs-tick-period 0.2))
	(td-macs--add creep (list (make-instance 'td-macs-modifier-slow :duration (seconds-to-time 0.40)
											 :slow 1)))
	(td-macs--entity-step creep)
	(should (= (td-macs-entity-speed creep) 1)) 

	(td-macs--entity-step creep)
	(should (= (td-macs-entity-speed creep) 1))

	;;duration over
	(td-macs--entity-step creep)
	(should (= (td-macs-entity-speed creep) 2))))

(ert-deftest td-macs-modifier-slow-cannot-make-speed-negative ()
  "Slow modifier is not allowed to make the speed negative."
  (let* ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0))))
		 (creep (make-td-macs-creep :speed 2))
		 (td-macs-tick-period 0.2))
	(td-macs--add creep (list (make-instance 'td-macs-modifier-slow :duration (seconds-to-time 0.40)
											 :slow 99)))
	(td-macs--entity-step creep)
	(should (= (td-macs-entity-speed creep) 0))))

(ert-deftest td-macs-tower-dmg-with-modifiers ()
  "When a tower with certain dmg-effect modifiers should apply the modifier on hit with the creep."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-path-code ,td-macs-path-code ,td-macs-fortress-code)
										 (,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code))
								:player (make-td-macs-player :pos (cons (list 0 1) 0))
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:dmg-effects `((poison :duration ,(seconds-to-time 1)))
																		:speed 1
																		:pos (cons (list 1 1) 0)))
								:creep-path (list '(0 0) '(1 0) '(2 0) '(3 0))
								:spawn (list 0 0))))
		(td-macs-tick-period 1.0))
	(td-macs-creeps-step)
	(should (cl-plusp (length (plist-get td-macs-game-state :creeps))))
	(td-macs--entity-step (car (plist-get td-macs-game-state :towers)))
	(should (seq-some 'td-macs-modifier-poison-p (td-macs-entity-modifiers (car (plist-get td-macs-game-state :creeps)))))))

(ert-deftest td-macs-dmg-effects-on-tower-shows-on-the-stats-buffer ()
  "DMG-Effects of the current selected tower are shown on the stats
buffer.  Atleast when the player is on the object."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-path-code))
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
										  :range '(1 1)
										  :dmg-effects `((poison :duration ,(seconds-to-time 1)))
										  :speed 1
										  :pos (cons (list 0 0) 0)))
								:player (make-td-macs-player :pos (cons (list 0 0) 0))))))
	(td-macs-with-temp-game-buffer
	  (td-macs--start-session-from-state state)
	  (save-excursion
		(with-current-buffer td-macs-stats-buffer-name
		  (goto-char (point-min))
		  (should (search-forward "Effects:" nil t)))))))

(ert-deftest td-macs-slow-upgrade-on-tower ()
  "When tower gets upgraded with a slow modifier/upgrade the tower
applies the modifier to the creep when hitting."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-path-code ,td-macs-path-code ,td-macs-fortress-code)
										 (,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code))
								:player (make-td-macs-player :pos (cons (list 1 1) 0))
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:speed 1
																		:pos-upg '((slow . 0))
																		:pos (cons (list 1 1) 0)))
								:creep-path (list '(0 0) '(1 0) '(2 0) '(3 0))
								:spawn (list 0 0))))
		(td-macs-tick-period 1.0))
	(td-macs-creeps-step)
	(should (cl-plusp (length (plist-get td-macs-game-state :creeps))))
	(td-macs--entity-step (car (plist-get td-macs-game-state :towers)))
	(should (not (seq-some 'td-macs-modifier-slow-p
						   (td-macs-entity-modifiers (car (plist-get td-macs-game-state :creeps))))))

	;;upgrade tower
	(execute-kbd-macro (kbd "M-x td-macs-tower-dispatch-menu <RET> q"))
	(td-macs--entity-step (car (plist-get td-macs-game-state :towers)))
	(should (seq-some 'td-macs-modifier-slow-p
						  (td-macs-entity-modifiers (car (plist-get td-macs-game-state :creeps)))))))

(ert-deftest td-macs-bomb-tower-slow-upgrade-removes-itself ()
  "When bomb-tower gets upgraded with a slow modifier/upgrade, it gets
removed from the pos-upg slot.  It can only be upgraded once."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code  ,td-macs-fortress-code)
										 (,td-macs-ground-code ,td-macs-ground-code))
								:player (make-td-macs-player :pos (cons (list 1 1) 0))
								:towers (list (make-td-macs-tower-bomb :dmg '(1 . 0)
																		:range '(1 1)
																		:speed 1
																		:pos-upg '((slow . 0) (poison . 5))
																		:pos (cons (list 1 1) 0)))))))
	(should (assoc 'slow (td-macs-tower-pos-upg (car (plist-get td-macs-game-state :towers)))))
	;;upgrade tower
	(execute-kbd-macro (kbd "M-x td-macs-tower-dispatch-menu <RET> q"))
	(should (equal '((poison . 5)) (td-macs-tower-pos-upg (car (plist-get td-macs-game-state :towers)))))))

(ert-deftest td-macs-bomb-tower-can-upgrade-to-slow ()
  "Bomb tower has slow upgrade... Don't know if this is a useful test"
  (should (assoc 'slow (td-macs-tower-pos-upg (make-td-macs-tower-bomb)))))

(ert-deftest td-macs-slow-modifier-changes-background-graphic ()
  "When slow modifier is applied it changes the background graphic."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-ground-code ,td-macs-ground-code))
								:creeps (list (make-td-macs-creep :pos (cons (list 0 0) 0)
																  :health 5))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0)
								:player (make-td-macs-player :pos (cons (list 2 0) 0)))))
		(td-macs-tick-period 1))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)
	 (td-macs-creeps-step)
	 (td-macs-game-draw td-macs-game-state)
	 (should (equal (list 'nil 'nil 'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max))))))
	 
	 (td-macs--add (car (plist-get td-macs-game-state :creeps))
				   (list (make-instance 'td-macs-modifier-slow :duration (seconds-to-time 99) :slow 99)))
	 (td-macs-game-draw td-macs-game-state)
	 (should (equal (list 'td-macs-entity-slow 'nil
						  'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max)))))))))

(ert-deftest td-macs-creep-path-straight ()
  "A level with straight creep path should produce a list with
coordinates that represent that straight line."
  (let ((level '((?# ?0 ?#)
				 (?# ?. ?#)
				 (?# ?. ?#)
				 (?# ?. ?#)
				 (?# ?X ?#))))
	(should (equal (td-macs-creep-path level)
				   '((1 0) (1 1)
					 (1 2) (1 3)
					 (1 4))))))

(ert-deftest td-macs-creep-path-with-corners ()
  "A creep path is allowed to have corners directly facing
norht/east/south/west.  No diagonal corners."
  (let ((level '((?# ?0 ?# ?#)
				 (?# ?. ?. ?#)
				 (?# ?# ?. ?.)
				 (?# ?# ?# ?.)
				 (?# ?X ?. ?.))))
	(should (equal (td-macs-creep-path level)
				   '((1 0) (1 1) (2 1)
					 (2 2)
					 (3 2) (3 3)
					 (3 4) (2 4)
					 (1 4))))))

(ert-deftest td-macs-creep-path-with-intersections ()
  "When a creep path has an intersection it should only follow the
one correct path."
  (let ((level '((?# ?# ?0 ?# ?#)
				 (?# ?# ?. ?# ?#)
				 (?X ?. (?. (0 1) (-1 0)) ?. ?.)
				 (?# ?# ?. ?# ?.)
				 (?# ?# ?. ?. ?.))))
	(should (equal (td-macs-creep-path level)
				   '((2 0) (2 1) (2 2)
					 (2 3) (2 4) (3 4)
					 (4 4) (4 3) (4 2)
					 (3 2) (2 2) (1 2)
					 (0 2))))))

(ert-deftest td-macs-level-is-stripped-down-at-start ()
  "When the level has other constructs then just a code at a
position.  It will be stripped down, to just codes, before the
game is started."
  (let ((td-macs-playable-levels '(("test-level" .
									(:waves td-macs-default-creep-waves
									 :level ((?# ?# ?0 ?# ?#)
											 (?# ?# ?. ?# ?#)
											 (?X ?. (?. (0 1) (-1 0)) ?. ?.)
											 (?# ?# ?. ?# ?.)
											 (?# ?# ?. ?. ?.))
									 :crystals 5)))))
	(td-macs-with-temp-game-buffer
	 (td-macs-start-session "test-level")
	 (should (equal (plist-get td-macs-game-state :level)
					'((?# ?# ?0 ?# ?#)
					  (?# ?# ?. ?# ?#)
					  (?X ?. ?. ?. ?.)
					  (?# ?# ?. ?# ?.)
					  (?# ?# ?. ?. ?.))))
	 (td-macs-quit-window))))

(ert-deftest td-macs-creep-moves-along-creep-path ()
  "Creeps should move along the creep path that was calculated."
  (let ((td-macs-playable-levels '(("test-level" .
									(:waves td-macs-default-creep-waves
									 :level ((?# ?# ?0 ?# ?#)
											 (?# ?# ?. ?# ?#)
											 (?X ?. (?. (0 1) (-1 0)) ?. ?.)
											 (?# ?# ?. ?# ?.)
											 (?# ?# ?. ?. ?.))
									 :crystals 5)))))
	(td-macs-with-temp-game-buffer
	 (td-macs-start-session "test-level")
	 (td-macs-creeps-step) ;;spawn creep
	 (let ((creep (car (plist-get td-macs-game-state :creeps)))
		   (td-macs-tick-period 1.0)) ;; so we move always
	   (dolist (pos (plist-get td-macs-game-state :creep-path))
		 (should (equal pos (td-macs--entity-pos creep)))
		 (td-macs--entity-step creep)))
	 (td-macs-quit-window))))

(ert-deftest td-macs-ask-to-save-score-when-losing ()
  "Prompt the player for saving the score when it lost the level."
  (let ((td-macs-playable-levels '(("test-level" .
									(:waves td-macs-default-creep-waves)))))
	(td-macs-with-temp-game-buffer
	 (td-macs-start-session "test-level")
	 (let ((inhibit-interaction t))
	  (should-error (execute-kbd-macro (kbd "q")) :type 'inhibited-interaction))
	 (td-macs-quit-window))))

(ert-deftest td-macs-slow-modifier-adds-itself-to-existing-modifier ()
  "When a entity already has a slow modifier. The slow modifier adds
itself to the slow modifier instead of having multiple slow
modifiers."
  (let* ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
										(list :level `((,td-macs-spawn-code))
											  :creep-path (list '(0 0) '(0 0))
											  :spawn (list 0 0))))
		 (creep (make-td-macs-creep :speed 15))
		 (td-macs-tick-period 0.2))
	(td-macs--add creep (list (make-instance 'td-macs-modifier-slow :duration (seconds-to-time 0.40)
														  :slow 5)
							  (make-instance 'td-macs-modifier-slow :duration (seconds-to-time 0.40)
														  :slow 1)
							  (make-instance 'td-macs-modifier-slow :duration (seconds-to-time 0.40)
														  :slow 4)))
	(td-macs--entity-step creep)
	(should (= 1 (length (td-macs-entity-modifiers creep))))
	(should (= (td-macs-entity-speed creep) 5))
	(should (= (time-to-seconds
					(td-macs-modifier-total-duration
					 (car (td-macs-entity-modifiers creep))))
			   1.20))

	(td-macs--add creep (list (make-instance 'td-macs-modifier-slow :duration (seconds-to-time 0.40)
														  :slow 4)))

	(td-macs--entity-step creep)
	(should (= 1 (length (td-macs-entity-modifiers creep))))
	(should (= (td-macs-entity-speed creep) 1))
		(should (= (time-to-seconds
					(td-macs-modifier-total-duration
					 (car (td-macs-entity-modifiers creep))))
			   1.60))))

(ert-deftest td-macs-modifier-slow-combined-slows-speed-and-returns-speed-back-after-duration-is-over ()
  "When the list of slow modifiers have been added it will slow the
speed of the entity.  However it needs to return back to the
original speed after duration is over."
  (let* ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0))))
		 (creep (make-td-macs-creep :speed 20))
		 (td-macs-tick-period 0.4))
	(td-macs--add creep (list (make-instance 'td-macs-modifier-slow :duration (seconds-to-time 0.40)
														  :slow 5)
							  (make-instance 'td-macs-modifier-slow :duration (seconds-to-time 0.40)
														  :slow 1)
							  (make-instance 'td-macs-modifier-slow :duration (seconds-to-time 0.40)
														  :slow 4)))
	(td-macs--entity-step creep)
	(should (= (td-macs-entity-speed creep) 10)) 

	(td-macs--entity-step creep)
	(should (= (td-macs-entity-speed creep) 10))

	;;duration over
	(td-macs--entity-step creep)
	(td-macs--entity-step creep)
	(should (= (td-macs-entity-speed creep) 20))))

(ert-deftest td-macs-increase-by-speed-should-not-update-class-slot ()
  "Updating a slot should not update the default shared slot, but only it own instance slot."
  (let ((tower-one (make-td-macs-tower :speed 1))
		(tower-two (make-td-macs-tower :speed 1))
		(td-macs-tick-period 0.016))
	(td-macs--incf-slot-by-speed tower-one td-macs-tower-dmg 1)
	(should (equal 0.016 (cdr (td-macs-tower-dmg tower-one))))
	(should (not (eq (td-macs-tower-dmg tower-one) (td-macs-tower-dmg tower-two))))))

(ert-deftest td-macs-tower-interest-is-shown-on-stats-buffer ()
  "Interest and speed is show on stats buffer when player is on tower."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-path-code))
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:speed 1
																		:pos (cons (list 0 0) 0)))
								:player (make-td-macs-player :pos (cons (list 0 0) 0))))))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)
	 (save-excursion
	   (with-current-buffer td-macs-stats-buffer-name
		 (goto-char (point-min))
		 (should (search-forward "Speed:" nil t))
		 (should (search-forward "Interest:" nil t)))))))

(ert-deftest td-macs-increase-own-speed-upgrade-on-tower ()
  "When a (dummy) tower gets upgraded with a speed modifier/upgrade the tower
applies the modifier to itself."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-path-code ,td-macs-path-code ,td-macs-fortress-code)
										 (,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code))
								:player (make-td-macs-player :pos (cons (list 1 1) 0))
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:speed 1
																		:pos-upg '((increase-own-speed . 0))
																		:pos (cons (list 1 1) 0)))
								:creep-path (list '(0 0) '(1 0) '(2 0) '(3 0))
								:spawn (list 0 0))))
		(td-macs-tick-period 1.0))
	(let ((tower (car (plist-get td-macs-game-state :towers))))
	  (td-macs--entity-step tower)
	 (should (not (seq-some 'td-macs-modifier-haste-p
							(td-macs-entity-modifiers tower))))

	 ;;upgrade tower
	 (execute-kbd-macro (kbd "M-x td-macs-tower-dispatch-menu <RET> q"))
	 (td-macs--entity-step tower)
	 (should (seq-some 'td-macs-modifier-haste-p
					   (td-macs-entity-modifiers tower)))
	 (should (> (td-macs-entity-speed tower) 1)))))

(ert-deftest td-macs-height-of-graphics-should-change ()
  "When a user scales the buffer the so called graphics should
change size too."
  (skip-unless (display-graphic-p))
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-path-code))
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:speed 1
																		:pos (cons (list 0 0) 0)))
								:player (make-td-macs-player :pos (cons (list 0 0) 0)))))
		(font-size (lambda ()
					 (font-get
					  (font-at
					   (apply 'td-macs--pos-to-point
							  (td-macs--entity-pos
							   (plist-get td-macs-game-state :player))))
					  :size))))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)
	 (save-excursion
	   (with-current-buffer td-macs-buffer-name
		 (let ((normal-size (funcall font-size)))
		   (text-scale-adjust 5)
		   (should (> (funcall font-size) normal-size))))))))

(ert-deftest td-macs-customizable-increase-scale-of-graphics ()
  "User are able to customize the default scale of the graphics.
This should be adhered to."
  (skip-unless (display-graphic-p))
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-path-code))
								:player (make-td-macs-player :pos (cons (list 0 0) 0)))))
		(font-size (lambda ()
					 (font-get
					  (font-at
					   (apply 'td-macs--pos-to-point
							  (td-macs--entity-pos
							   (plist-get td-macs-game-state :player))))
					  :size)))
		(initial-size nil))
	(let ((td-macs-default-graphics-scale 2))
	  (td-macs-with-temp-game-buffer
	   (td-macs--start-session-from-state state)
	   (save-excursion
		 (with-current-buffer td-macs-buffer-name
		   (setq initial-size (funcall font-size))))))

	(let ((td-macs-default-graphics-scale 5))
	  (td-macs-with-temp-game-buffer
	   (td-macs--start-session-from-state state)
	   (save-excursion
		 (with-current-buffer td-macs-buffer-name
		   (should (> (funcall font-size) initial-size))))))))

(ert-deftest td-macs-customizable-decrease-scale-of-graphics ()
  "User are able to customize the default scale of the graphics.
This should be adhered to."
  (skip-unless (display-graphic-p))
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-path-code))
								:player (make-td-macs-player :pos (cons (list 0 0) 0)))))
		(font-size (lambda ()
					 (font-get
					  (font-at
					   (apply 'td-macs--pos-to-point
							  (td-macs--entity-pos
							   (plist-get td-macs-game-state :player))))
					  :size)))
		(initial-size nil))
	(let ((td-macs-default-graphics-scale 2))
	  (td-macs-with-temp-game-buffer
	   (td-macs--start-session-from-state state)
	   (save-excursion
		 (with-current-buffer td-macs-buffer-name
		   (setq initial-size (funcall font-size))))))

	(let ((td-macs-default-graphics-scale 1))
	  (td-macs-with-temp-game-buffer
	   (td-macs--start-session-from-state state)
	   (save-excursion
		 (with-current-buffer td-macs-buffer-name
		   (should (< (funcall font-size) initial-size))))))))

(ert-deftest td-macs-buff-tower-hits-surrounding-towers-instead-of-creeps ()
  "The buff tower, when it does the dmg hit, will select towers instead of creeps."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-path-code ,td-macs-path-code ,td-macs-fortress-code)
										 (,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code))
								:player (make-td-macs-player :pos (cons (list 1 1) 0))
								:creeps (list (make-td-macs-creep :health 4
																  :pos (cons (list 2 0) 0)))
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:speed 1
																		:health 4
																		:pos (cons (list 1 1) 0))
											  (make-td-macs-tower-buff :dmg '(1 . 0)
																	   :range '(1 1)
																	   :speed 1
																	   :pos (cons (list 2 1) 0)))
								:spawn (list 0 0))))
		(td-macs-tick-period 1.0))
	(let ((buff-tower (seq-find 'td-macs-tower-buff-p (plist-get td-macs-game-state :towers)))
		  (dummy-tower (seq-find 'td-macs-tower-dummy-p (plist-get td-macs-game-state :towers))))
	  (td-macs--entity-step buff-tower)
	 (should (= 3 (td-macs-entity-health dummy-tower)))
	 (should (= 4 (td-macs-entity-health (car (plist-get td-macs-game-state :creeps))))))))

(ert-deftest td-macs-buff-tower-buffs-speed-on-nearby-towers ()
  "The buff tower, when it does the dmg hit, will buff nearby towers with speed."
  (let ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
									   (list :level `((,td-macs-spawn-code ,td-macs-path-code ,td-macs-path-code ,td-macs-fortress-code)
													  (,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code))
											 :player (make-td-macs-player :pos (cons (list 2 1) 0))
											 :creeps (list (make-td-macs-creep :health 4
																			   :pos (cons (list 2 0) 0)))
											 :towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																					 :range '(1 1)
																					 :speed 1
																					 :health 4
																					 :pos (cons (list 1 1) 0))
														   (make-td-macs-tower-buff :dmg '(0 . 0)
																					:range '(1 1)
																					:speed 1
																					:pos-upg '((speed . 0))
																					:pos (cons (list 2 1) 0)))
											 :spawn (list 0 0))))
		(td-macs-tick-period 1.0))
	(let ((buff-tower (seq-find 'td-macs-tower-buff-p (plist-get td-macs-game-state :towers)))
		  (dummy-tower (seq-find 'td-macs-tower-dummy-p (plist-get td-macs-game-state :towers))))
	  (should (not (seq-some 'td-macs-modifier-haste-p
							 (td-macs-entity-modifiers dummy-tower))))
	  (should (= 1 (td-macs-entity-speed dummy-tower)))

	  (execute-kbd-macro (kbd "M-x td-macs-tower-dispatch-menu <RET> q"))
	  (td-macs--entity-step buff-tower)
	  
	  (should (seq-some 'td-macs-modifier-haste-p
						(td-macs-entity-modifiers dummy-tower)))
	  (should (= 2 (td-macs-entity-speed dummy-tower))))))

(ert-deftest td-macs-buff-tower-highlights-haste-buffed-towers ()
  "When the speed buff is applied to the towers change the display
of the towers."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-path-code ,td-macs-path-code ,td-macs-fortress-code)
										 (,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code))
								:player (make-td-macs-player :pos (cons (list 0 1) 0))
								:creeps (list (make-td-macs-creep :health 4
																  :pos (cons (list 2 0) 0)))
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:speed 1
																		:health 4
																		:pos (cons (list 1 1) 0))
											  (make-td-macs-tower-buff :dmg '(0 . 0)
																	   :range '(1 1)
																	   :speed 1
																	   :dmg-effects '((haste
																					   :duration (seconds-to-time 1.5)
																					   :haste 1))
																	   :pos (cons (list 2 1) 0)))
								:spawn (list 0 0))))
		(td-macs-tick-period 1.0))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)
	 (td-macs-game-draw td-macs-game-state)
	 (should (equal (list nil nil nil nil nil
						  nil nil nil nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max))))))
	
	 (let ((buff-tower (seq-find 'td-macs-tower-buff-p (plist-get td-macs-game-state :towers))))
	   (td-macs--entity-step buff-tower)
	   (td-macs-game-draw td-macs-game-state)
	   (should (equal (list nil nil nil nil nil
							nil '(td-macs-entity-haste td-macs-entity-hit) nil nil nil)
					  (seq-map (lambda (pos) (get-text-property pos 'face))
							   (td-macs-range (point-min) (1- (point-max))))))))))

(ert-deftest td-macs-spawn-check-should-correctly-tell-if-it-is-when-player-is-on-spawn ()
  "Creeps should only spawn if there are no creeps on the spawn.
Even if the player is on the spawn also."
  (let* ((level `((,td-macs-spawn-code ,td-macs-path-code ,td-macs-path-code ,td-macs-fortress-code)
										  (,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code ,td-macs-ground-code)))
		 (state (map-merge 'plist (td-macs--game-state-default)
						   (list :level level
								 :creep-path (td-macs-creep-path level)
								 :player (make-td-macs-player :pos (cons (list 0 0) 0))
								 :towers '()
								 :spawn (list 0 0))))
		(td-macs-tick-period 0.25))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)
	 (td-macs-creeps-step)
	 
	 (td-macs-with-plist (creeps) td-macs-game-state
	   (should (= 1 (length creeps)))
	   (should (equal '(0 0) (td-macs--entity-pos (car creeps))))

	   (td-macs-creeps-step)
	   (td-macs-creeps-step)
	   (td-macs-creeps-step)
	   (should (= 1 (length creeps)))

	   (td-macs-creeps-step)
	   (should (= 2 (length creeps)))
	   (should (equal '((0 0) (1 0)) (seq-map 'td-macs--entity-pos creeps)))

	   (td-macs-creeps-step)
	   (td-macs-creeps-step)
	   (td-macs-creeps-step)
	   (should (= 2 (length creeps)))

	   (td-macs-creeps-step)
	   (should (= 3 (length creeps)))
	   (should (equal '((0 0) (1 0) (2 0)) (seq-map 'td-macs--entity-pos creeps)))))))


(ert-deftest td-macs-creep-multiple-spawns ()
  ""
  (should (= 0 1)))

(ert-deftest td-macs-haste-modifier-combines-for-longer-duration ()
  "When the haste modifier gets added to a entity it should combine
instead of being multiple modifiers.  In the process it should
add the durations together."
  (let* ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
										(list :level `((,td-macs-spawn-code))
											  :creep-path (list '(0 0) '(0 0))
											  :spawn (list 0 0))))
		 (tower (make-td-macs-tower-dummy :speed 15))
		 (creep (make-td-macs-creep :speed 15))
		 (td-macs-tick-period 0.2))
	(td-macs--add creep (list (make-instance 'td-macs-modifier-haste :duration (seconds-to-time 0.40)
											 :haste 1)
							  (make-instance 'td-macs-modifier-haste :duration (seconds-to-time 0.40)
											 :haste 1)))
	(td-macs--add tower (list (make-instance 'td-macs-modifier-haste :duration (seconds-to-time 0.40)
											 :haste 1)
							  (make-instance 'td-macs-modifier-haste :duration (seconds-to-time 0.40)
											 :haste 1)))

	(mapcar 'td-macs--entity-step (list creep tower))
	(should (= 1 (length (td-macs-entity-modifiers creep)) (length (td-macs-entity-modifiers tower))))
	(should (= (time-to-seconds
					(td-macs-modifier-total-duration
					 (car (td-macs-entity-modifiers creep))))
			   (time-to-seconds
					(td-macs-modifier-total-duration
					 (car (td-macs-entity-modifiers tower))))
			   0.8))

	(td-macs--add creep (list (make-instance 'td-macs-modifier-haste :duration (seconds-to-time 0.40)
											 :haste 4)))

	(td-macs--add tower (list (make-instance 'td-macs-modifier-haste :duration (seconds-to-time 0.40)
											 :haste 4)))

	(mapcar 'td-macs--entity-step (list creep tower))
	
	(should (= 1 (length (td-macs-entity-modifiers creep)) (length (td-macs-entity-modifiers tower))))
	(should (= (time-to-seconds
					(td-macs-modifier-total-duration
					 (car (td-macs-entity-modifiers creep))))
			   (time-to-seconds
					(td-macs-modifier-total-duration
					 (car (td-macs-entity-modifiers tower))))
			   1.2))))

(ert-deftest td-macs-haste-modifier-combines-but-only-takes-highest-speed ()
  "When multiple haste modifiers get added to an entity it should
only increase the speed by the highest haste modifier."
  (let* ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
										(list :level `((,td-macs-spawn-code))
											  :creep-path (list '(0 0) '(0 0))
											  :spawn (list 0 0))))
		 (tower (make-td-macs-tower-dummy :speed 15))
		 (creep (make-td-macs-creep :speed 15))
		 (td-macs-tick-period 0.2))
	(td-macs--add creep (list (make-instance 'td-macs-modifier-haste :duration (seconds-to-time 0.40)
											 :haste 2)
							  (make-instance 'td-macs-modifier-haste :duration (seconds-to-time 0.40)
											 :haste 6)))
	(td-macs--add tower (list (make-instance 'td-macs-modifier-haste :duration (seconds-to-time 0.40)
											 :haste 5)
							  (make-instance 'td-macs-modifier-haste :duration (seconds-to-time 0.40)
											 :haste 6)
							  (make-instance 'td-macs-modifier-haste :duration (seconds-to-time 0.40)
											 :haste 2)))

	(mapcar 'td-macs--entity-step (list creep tower))
	(should (= 1 (length (td-macs-entity-modifiers creep)) (length (td-macs-entity-modifiers tower))))
	(should (= 6 (td-macs-modifier-haste-haste (car (td-macs-entity-modifiers creep)))
			   (td-macs-modifier-haste-haste (car (td-macs-entity-modifiers tower)))))

	(should (= 21 (td-macs-entity-speed creep)
			   (td-macs-entity-speed tower)))

	(td-macs--add creep (list (make-instance 'td-macs-modifier-haste :duration (seconds-to-time 0.40)
											 :haste 2)))

	(td-macs--add tower (list (make-instance 'td-macs-modifier-haste :duration (seconds-to-time 0.40)
											 :haste 3)))

	(mapcar 'td-macs--entity-step (list creep tower))
	
	(should (= 1 (length (td-macs-entity-modifiers creep)) (length (td-macs-entity-modifiers tower))))
	(should (= 6 (td-macs-modifier-haste-haste (car (td-macs-entity-modifiers creep)))
			   (td-macs-modifier-haste-haste (car (td-macs-entity-modifiers tower)))))
	(should (= 21 (td-macs-entity-speed creep)
			   (td-macs-entity-speed tower)))))

(ert-deftest td-macs-buff-tower-prefers-targets-with-the-least-modifiers ()
  "When the buff tower determines its targets it should prefer to target towers with the least modifiers."
  (let* ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
										(list :level `((,td-macs-spawn-code ,td-macs-ground-code)
													   (,td-macs-ground-code ,td-macs-ground-code)
													   (,td-macs-ground-code ,td-macs-ground-code))
											  :creep-path (list '(0 0) '(0 0))
											  :towers (list (make-td-macs-tower-dummy :pos (cons (list 0 1) 0))
															(make-td-macs-tower-dummy :pos (cons (list 0 2) 0))
															(make-td-macs-tower-dummy :pos (cons (list 1 0) 0))
															(make-td-macs-tower-dummy :pos (cons (list 1 2) 0)))
											  :spawn (list 0 0))))
		 (buff-tower (make-td-macs-tower-buff :speed 1
										 :dmg-effects (list (list 'haste
																  :duration (seconds-to-time 99)
																  :haste 1))
										 :range '(1 1)
										 :pos (cons (list 1 1) 0)))
		 (td-macs-tick-period 1)
		 (amount-of-haste-mods (lambda (tower) (= 1 (length (seq-filter 'td-macs-modifier-haste-p
																			 (td-macs-entity-modifiers tower)))))))
	(should (= 0 (seq-reduce (lambda (sum tower) (+ sum (length (td-macs-entity-modifiers tower))))
							 (plist-get td-macs-game-state :towers)
							 0)))
	
	;; every step we retarget to new target
	(td-macs--entity-step buff-tower)
	(should (= 1 (length (seq-filter amount-of-haste-mods
							 (plist-get td-macs-game-state :towers)))))

	(td-macs--entity-step buff-tower)
	(should (= 2 (length (seq-filter amount-of-haste-mods
							 (plist-get td-macs-game-state :towers)))))

	(td-macs--entity-step buff-tower)
	(should (= 3 (length (seq-filter amount-of-haste-mods
							 (plist-get td-macs-game-state :towers)))))

	(td-macs--entity-step buff-tower)
	(should (= 4 (length (seq-filter amount-of-haste-mods
							 (plist-get td-macs-game-state :towers)))))))

(ert-deftest td-macs-tower-hits-with-poison-that-changes-the-creep-graphic ()
  "When the tower hits a creep the graphic changes to show it was hit."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-ground-code ,td-macs-ground-code))
								:creeps (list (make-td-macs-creep :pos (cons (list 0 0) 0)
																  :health 5))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0)
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:speed 1
																		:dmg-effects '((poison :dmg 10
																							   :duration (seconds-to-time 10)))
																		:pos (cons (list 1 0) 0)))
								:player (make-td-macs-player :pos (cons (list 2 0) 0)))))
		(td-macs-tick-period 1))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)
	 (should (equal (list 'nil 'nil 'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max))))))
	 (td-macs-creeps-step)
	 (td-macs-towers-step)
	 (td-macs-game-draw td-macs-game-state)
	 (should (equal (list '(td-macs-entity-poison td-macs-entity-hit) 'nil
						  'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max)))))))))

(ert-deftest td-macs-tower-upgrade-poison-should-apply-the-dmg-effect ()
  "When the tower hits a creep the poison modifier should be applied
to the creep."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-ground-code ,td-macs-ground-code))
								:creeps (list (make-td-macs-creep :pos (cons (list 0 0) 0)
																  :health 5))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0)
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:speed 1
																		:pos-upg '((poison . 0))
																		:pos (cons (list 1 0) 0)))
								:player (make-td-macs-player :pos (cons (list 1 0) 0)))))
		(td-macs-tick-period 1))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)	
	 (td-macs-creeps-step)
	 (td-macs-towers-step)
	 (should (seq-empty-p (td-macs-tower-dmg-effects (car (plist-get td-macs-game-state :towers)))))

	 (execute-kbd-macro (kbd "M-x td-macs-tower-dispatch-menu <RET> q"))
	 (should (assoc 'poison
					(td-macs-tower-dmg-effects (car (plist-get td-macs-game-state :towers)))))

	 (td-macs-towers-step)
	 (should (seq-some 'td-macs-modifier-poison-p
			   (td-macs-entity-modifiers (car (plist-get td-macs-game-state :creeps))))))))

(ert-deftest td-macs-fire-modifier-should-add-fire-dmg-type ()
  "Fire modifier should add the fire dmg type to the tower that it
is added to."
  (let* ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
										(list :level `((,td-macs-spawn-code ,td-macs-ground-code)
													   (,td-macs-ground-code ,td-macs-ground-code))
											  :creep-path (list '(0 0) '(0 0))
											  :towers (list (make-td-macs-tower-dummy :pos (cons (list 1 1) 0)))
											  :spawn (list 0 0))))
		 (td-macs-tick-period 1))
	(should (equal '(normal) (td-macs-tower-dmg-types (car (plist-get td-macs-game-state :towers)))))

	(td-macs--add (car (plist-get td-macs-game-state :towers))
				  (list (make-instance 'td-macs-modifier-fire :duration (seconds-to-time 10))))
	(td-macs--entity-step (car (plist-get td-macs-game-state :towers)))
	(should (equal '(fire normal) (td-macs-tower-dmg-types (car (plist-get td-macs-game-state :towers)))))))

(ert-deftest td-macs-fire-modifier-fire-dmg-type-should-be-removed-after-duration-is-over ()
  "After the duration is over the modifier should remove the fire
damage type from the tower."
  (let* ((td-macs-game-state (map-merge 'plist (td-macs--game-state-default)
										(list :level `((,td-macs-spawn-code ,td-macs-ground-code)
													   (,td-macs-ground-code ,td-macs-ground-code))
											  :creep-path (list '(0 0) '(0 0))
											  :towers (list (make-td-macs-tower-dummy :pos (cons (list 1 1) 0)))
											  :spawn (list 0 0))))
		 (td-macs-tick-period 1))
	(td-macs--add (car (plist-get td-macs-game-state :towers))
				  (list (make-instance 'td-macs-modifier-fire :duration (seconds-to-time 4))))

	(td-macs--entity-step (car (plist-get td-macs-game-state :towers)))
	(should (equal '(fire normal) (td-macs-tower-dmg-types (car (plist-get td-macs-game-state :towers)))))

	(td-macs--entity-step (car (plist-get td-macs-game-state :towers)))
	(td-macs--entity-step (car (plist-get td-macs-game-state :towers)))
	(td-macs--entity-step (car (plist-get td-macs-game-state :towers)))
	(should (equal '(fire normal) (td-macs-tower-dmg-types (car (plist-get td-macs-game-state :towers)))))

	(td-macs--entity-step (car (plist-get td-macs-game-state :towers)))
	(should (equal '(normal) (td-macs-tower-dmg-types (car (plist-get td-macs-game-state :towers)))))))

(ert-deftest td-macs-fire-modifier-graphics-changes-over-time ()
  "The fire modifier changes the graphics of the entity it is applied to."
    (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-ground-code ,td-macs-ground-code))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0)
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:speed 1
																		:pos (cons (list 1 0) 0)))
								:player (make-td-macs-player :pos (cons (list 2 0) 0)))))
		(td-macs-tick-period 1))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)
	 (should (equal (list 'nil 'nil 'nil nil)
					(seq-map (lambda (pos) (get-text-property pos 'face))
							 (td-macs-range (point-min) (1- (point-max))))))
	 (td-macs--add (car (plist-get td-macs-game-state :towers))
				  (list (make-instance 'td-macs-modifier-fire :duration (seconds-to-time 15)
									   :faces '(td-macs-entity-hit diary match))))

	 (dolist (face '(td-macs-entity-hit diary match td-macs-entity-hit
										diary match td-macs-entity-hit))
	   (td-macs-towers-step)
	   (td-macs-game-draw td-macs-game-state)
	   (should (equal (list 'nil face
							'nil nil)
					  (seq-map (lambda (pos) (get-text-property pos 'face))
							   (td-macs-range (point-min) (1- (point-max))))))))))

(ert-deftest td-macs-fire-modifier-is-upgradable-from-tower ()
  "Fire modifier should be able to be added via the upgrade menu."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-ground-code ,td-macs-ground-code))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0)
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:speed 1
																		:pos-upg '((fire . 0))
																		:pos (cons (list 1 0) 0)))
								:player (make-td-macs-player :pos (cons (list 1 0) 0)))))
		(td-macs-tick-period 1))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)	
	 (td-macs-towers-step)
	 (should (equal '(normal) (td-macs-tower-dmg-types (car (plist-get td-macs-game-state :towers)))))
	 (should (seq-empty-p (td-macs-entity-modifiers (car (plist-get td-macs-game-state :towers)))))

	 (execute-kbd-macro (kbd "M-x td-macs-tower-dispatch-menu <RET> q"))
	 (should (seq-some 'td-macs-modifier-fire-p
					(td-macs-entity-modifiers (car (plist-get td-macs-game-state :towers)))))
	 (should (equal '(fire normal)
					(td-macs-tower-dmg-types (car (plist-get td-macs-game-state :towers))))))))

(ert-deftest td-macs-fire-dmg-type-hits-creeps-harder-by-default ()
  "Fire hits default creeps harder unless it is specified."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-ground-code ,td-macs-ground-code))
								:creeps (list (make-td-macs-creep :pos (cons (list 0 0) 0)
																  :health 5))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0)
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:dmg-types '(fire normal)
																		:speed 1
																		:pos (cons (list 1 0) 0)))
								:player (make-td-macs-player :pos (cons (list 1 0) 0)))))
		(td-macs-tick-period 1))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)

	 (should (= 5 (td-macs-entity-health (car (plist-get td-macs-game-state :creeps)))))
	 (td-macs-creeps-step)
	 (td-macs-towers-step)
	 (should (= 3.8 (td-macs-entity-health (car (plist-get td-macs-game-state :creeps))))))))

(ert-deftest td-macs-fire-dmg-type-hits-creeps-based-on-the-resistance ()
  "Fire hits creeps with resistance according to the resistance."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((,td-macs-spawn-code ,td-macs-ground-code ,td-macs-ground-code))
								:creeps (list (make-td-macs-creep :pos (cons (list 0 0) 0)
																  :resistances '((fire . 10))
																  :health 5))
								:creep-path (list '(0 0) '(0 0))
								:spawn (list 0 0)
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:dmg-types '(fire normal)
																		:speed 1
																		:pos (cons (list 1 0) 0)))
								:player (make-td-macs-player :pos (cons (list 1 0) 0)))))
		(td-macs-tick-period 1))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)

	 (should (= 5 (td-macs-entity-health (car (plist-get td-macs-game-state :creeps)))))
	 (td-macs-creeps-step)
	 (td-macs-towers-step)
	 (should (= 4.1 (td-macs-entity-health (car (plist-get td-macs-game-state :creeps))))))))

(ert-deftest td-macs-display-damage-dealt-for-hit-modifier-on-stats-buffer ()
  "Instead of only displaying the duration, also display the damage that is dealt via the hit modifier."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((?0 ?# ?#))
								:creeps (list (make-td-macs-creep :pos (cons (list 0 0) 0)
																  :resistances '((fire . 10))
																  :path-pos 0
																  :health 5))
								:creep-path (list '(0 0) '(0 0) '(0 0))
								:spawn (list 0 0)
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:dmg-types '(fire normal)
																		:speed 1
																		:pos (cons (list 1 0) 0)))
								:player (make-td-macs-player :pos (cons (list 0 0) 0)))))
		(td-macs-tick-period 1))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)

	 (td-macs-creeps-step)
	 (td-macs-game-draw td-macs-game-state)
	 (with-current-buffer td-macs-stats-buffer-name
	   (goto-char (point-min))
	   (should (not (re-search-forward (rx "Hit, Duration: " (one-or-more digit) "." (one-or-more digit) ", Damage: 0.9") nil t))))
	 
	 (td-macs-towers-step)
	 (td-macs-creeps-step)
	 (td-macs-game-draw td-macs-game-state)
	 (with-current-buffer td-macs-stats-buffer-name
	   (goto-char (point-min))
	   (should (re-search-forward (rx "Hit, Duration: " (one-or-more digit) "." (one-or-more digit) ", Damage: 0.9") nil t))))))

(ert-deftest td-macs-start-session-should-error-when-no-level-is-selected ()
  "When the level selected does not exist, it should throw an error."
  (let ((td-macs-playable-levels '(("test-level" .
									(:waves td-macs-default-creep-waves
									 :level td-macs-default-map
									 :crystals 5)))))
	(should-error (td-macs-start-session "non-existent-level"))))

(ert-deftest td-macs-start-session-should-not-error-a-valid-level-is-selected ()
  "When the level selected does exist, it should load the game buffers."
  (let ((td-macs-playable-levels '(("test-level" .
									(:waves td-macs-default-creep-waves
									 :level td-macs-default-map
									 :crystals 5)))))
	(td-macs-with-temp-game-buffer
	 (td-macs-start-session "test-level")
	 (should (and (buffer-live-p (get-buffer td-macs-stats-buffer-name))
				  (buffer-live-p (get-buffer td-macs-buffer-name))))
	 (td-macs-quit-window))))

(ert-deftest td-macs-display-dps-dealt-by-poison-modifier-on-stats-buffer ()
  "Instead of only displaying the duration, also display the damage
per second that is dealt via the poison modifier."
  (let ((state (map-merge 'plist (td-macs--game-state-default)
						  (list :level `((?0 ?# ?#))
								:creeps (list (make-td-macs-creep :pos (cons (list 0 0) 0)
																  :resistances '((fire . 10))
																  :path-pos 0
																  :health 5))
								:creep-path (list '(0 0) '(0 0) '(0 0))
								:spawn (list 0 0)
								:towers (list (make-td-macs-tower-dummy :dmg '(1 . 0)
																		:range '(1 1)
																		:dmg-effects `((poison :dmg 20
																							   :duration ,(seconds-to-time 10)))
																		:speed 1
																		:pos (cons (list 1 0) 0)))
								:player (make-td-macs-player :pos (cons (list 0 0) 0)))))
		(td-macs-tick-period 1))
	(td-macs-with-temp-game-buffer
	 (td-macs--start-session-from-state state)

	 (td-macs-creeps-step)
	 (td-macs-game-draw td-macs-game-state)	 
	 (with-current-buffer td-macs-stats-buffer-name
	   (goto-char (point-min))
	   (should (not (re-search-forward (rx "Poison, Duration: " (one-or-more digit) "." (one-or-more digit) ", DPS: 2.0, Total: 20") nil t))))

	 (td-macs-towers-step)
	 (td-macs-creeps-step)
	 (td-macs-game-draw td-macs-game-state)
	 (with-current-buffer td-macs-stats-buffer-name
	   (goto-char (point-min))
	   (should (re-search-forward (rx "Poison, Duration: " (one-or-more digit) "." (one-or-more digit) ", DPS: 2.0, Total: 20") nil t))))))

(provide 'td-macs-tests)

;;; td-macs-tests.el ends here
