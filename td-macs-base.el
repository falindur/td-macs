;;; package --- td-macs-base  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:
(mapcar #'require '(eieio cl-macs cl-lib gamegrid map seq))

(defcustom td-macs-image-load-path (file-name-directory (macroexp-file-name))
  "File location where the images/glyphs are stored."
  :type 'directory)

(defun td-macs--make-display-option (draw-char &optional image-file-spec)
  "IMAGE-FILE-SPEC is represented as '(FILE-NAME . IMAGE-TYPE)"
  `((,(when image-file-spec
		`(glyph ((:type ,(cdr image-file-spec)
						:file ,(concat td-macs-image-load-path (car image-file-spec)))))) 
	 (t ,draw-char))
	((color-x color-tty)
	 (mono-x mono-x)
     (color-tty color-tty))
	() ;; currently not using colours
	;; (((glyph color-x) "green")
     ;; (color-tty "green"))
	))

(defun td-macs-pos-x (pos) (car pos))
(defun td-macs-pos-y (pos) (cadr pos))

(defun td-macs--level-inner-bounds (level)
  (list (max 0 (1- (length (car level)))) (max 0 (1- (length level)))))

(defun td-macs--bounds-min-x (bounds)
  "Returns the min x coordinate part of the bounds.  Cropped to zero when the original bounds would have been negative."
  (max 0 (- (caar bounds) (caadr bounds))))

(defun td-macs--bounds-max-x (bounds)
  "Returns the max x coordinate part of the bounds.  Cropped to `td-macs--level-inner-bounds'
when the original bounds would have been out of range."
  (td-macs-with-plist (level) td-macs-game-state
	(min
	 (td-macs-pos-x (td-macs--level-inner-bounds level))
	 (+ (caar bounds) (caadr bounds)))))

(defun td-macs--bounds-min-y (bounds)
  "Returns the min y coordinate part of the bounds.  Cropped to zero when the original bounds would have been negative."
  (max 0 (- (cadar bounds) (cadadr bounds))))

(defun td-macs--bounds-max-y (bounds)
  "Returns the max y coordinate part of the bounds.  Cropped to `td-macs--level-inner-bounds'
when the original bounds would have been out of range."
  (td-macs-with-plist (level) td-macs-game-state
   (min
	(td-macs-pos-y (td-macs--level-inner-bounds level))
	(+ (cadar bounds) (cadadr bounds)))))

(defun td-macs-within-bounds-p (pos bounds)
  (let ((x (td-macs-pos-x pos))
		(y (td-macs-pos-y pos)))
	(and (<= (td-macs--bounds-min-x bounds) x (td-macs--bounds-max-x bounds))
		 (<= (td-macs--bounds-min-y bounds) y (td-macs--bounds-max-y bounds)))))

(defmacro td-macs-*% (number percentage)
  "Multiply NUMBER by the PERCENTAGE.
Returns the calculated value as a result.
Example:
(td-macs-*% 5 95) => 4.75"
  `(/ (* ,number ,percentage) 100.0))

(defun td-macs-range (start end)
  "Returns a LIST of integers from inclusive START and END, inclusive."
  (named-let td-macs--range ((actual-start (min start end))
							 (actual-end (max start end)))
	(cond ((> actual-start actual-end) '())
		  (t (cons actual-start (td-macs--range (1+ actual-start) actual-end))))))

(defmacro td-macs-with-plist (properties plist &rest body)
  "Bind PROPERTIES to the corresponding values, plist-get form, of PLIST.  
After execute the body."
  (declare (indent defun))
  (require 'cl-lib)
  `(cl-symbol-macrolet
	   ,(mapcar (lambda (prop)
				  `(,prop (plist-get ,plist ,(intern (concat ":" (symbol-name prop))))))
                properties)
	 ,@body))

(cl-defgeneric td-macs--draw (entity)
  "Protocol for everything that wants to draw something.")

(provide 'td-macs-base)

;;; td-macs-base.el ends here
